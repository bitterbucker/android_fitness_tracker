package com.rawfit.me.stepcounter;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.rawfit.me.main.PagerAdapter;
import com.rawfit.me.main.ProfileActivity;

import com.rawfit.me.main.R;
import com.rawfit.me.main.User;
import com.squareup.leakcanary.LeakCanary;

import java.io.Serializable;

import static com.rawfit.me.main.Dashboard.animate;
import static com.rawfit.me.main.Dashboard.initLeakCanary;

//https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html - about float
public class StepCounterActivity extends AppCompatActivity {

    private static final String TAG = "StepCounterActivity";

    //user profile
    public static User user;

    public ViewPager viewPager;
    public PagerAdapter adapter;
    public TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLeakCanary(getApplication(), this);




        setContentView(R.layout.content_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //animation
        animate(getWindow());

        user = (User) getIntent().getSerializableExtra("userInfo");
        //Log.d(TAG, user.toString());


        //tabs layout
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Today"));
        tabLayout.addTab(tabLayout.newTab().setText("Statistics"));
        tabLayout.addTab(tabLayout.newTab().setText("Achievements"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) findViewById(R.id.pager);
        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        viewPager.setOffscreenPageLimit(2);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //Log.d(TAG, "Tab changed");
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //Log.d(TAG, "Tab changed");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //Log.d(TAG, "Tab changed");
            }


        });

    }


    @Override
    public void onResume(){
        super.onResume();

        //Log.d(TAG, "StepcounterACtivity resumed");
    }

    @Override
    public void onPause(){
        super.onPause();

        //Log.d(TAG, "StepcounterACtivity paused");
    }

    /*@Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.stepcountermenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.info) {

            Intent intent = new Intent(this, Info.class);
            startActivity(intent);

            return true;
        }
        if (id == R.id.profile_button) {
            Intent intent = new Intent(this, ProfileActivity.class);

            intent.putExtra("userInfo", (Serializable) user);

            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




    //TODO: fix input weight and unit conversion
    public static int calsPerStep(User user, double steps) {
        double weight = 0;

        if(user.getUnit().equals("metric")){
            weight = user.getWeight() * 2.2;
        }
        else{
            weight = user.getWeight();
        }

        double stepsPerMile = 2200;

        double caloriesBurnedPerMile = 0.57 * weight;

        double conversionFactor = caloriesBurnedPerMile / stepsPerMile;

        double caloriesBurned = steps * conversionFactor;

        return (int) caloriesBurned;
    }

}
