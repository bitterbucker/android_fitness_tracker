package com.rawfit.me.stepcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.rawfit.me.main.R;


public class Info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        getSupportActionBar().setTitle("Info");

    }
}
