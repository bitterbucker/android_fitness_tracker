package com.rawfit.me.stepcounter;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Achievement {

    private String typeOfAchievement;
    private int currentGoalWhenAchieved;
    private long timestamp;

    public Achievement(String typeOfAchievement, int currentGoalWhenAchieved, long timestamp) {
        this.typeOfAchievement = typeOfAchievement;
        this.currentGoalWhenAchieved = currentGoalWhenAchieved;
        this.timestamp = timestamp;
    }

    public String getTypeOfAchievement() {
        return typeOfAchievement;
    }

    public void setTypeOfAchievement(String typeOfAchievement) {
        this.typeOfAchievement = typeOfAchievement;
    }

    public int getCurrentGoalWhenAchieved() {
        return currentGoalWhenAchieved;
    }

    public void setCurrentGoalWhenAchieved(int currentGoalWhenAchieved) {
        this.currentGoalWhenAchieved = currentGoalWhenAchieved;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {

        String ts = null;

        if(typeOfAchievement.equals("DSG")) {
            ts = "Daily Goal: " + currentGoalWhenAchieved + " achieved on " + new SimpleDateFormat("dd/MM/YYYY").format(new Date(timestamp));
        }
        else {
            ts = "Daily Goal: " + currentGoalWhenAchieved + " achieved on " + new SimpleDateFormat("dd/MM/YYYY").format(new Date(timestamp));
        }

        return ts;
    }
}
