package com.rawfit.me.stepcounter;



public class AggregationValue {
    private int date; // date in numbers, m d w
    private int value;
    private long timestamp;

    private Object extra;

    public AggregationValue(int date, int value) {
        this.date = date;
        this.value = value;
    }

    public AggregationValue(int date, int value, long timestamp) {
        this.date = date;
        this.value = value;
        this.timestamp = timestamp;
    }

    public AggregationValue(){

    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }



    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }
}
