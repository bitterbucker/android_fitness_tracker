package com.rawfit.me.stepcounter;

import java.io.Serializable;



public class AggregateInformation implements Serializable{

    private String type;

    private int date; //day number, week number or month number of the YEAR

    private int totalSteps;
    private int calsBurned;
    private int healthyPaceSteps;
    private int healthyPaceDUration;
    private double distance;

    public double getTotalStepsForMandW() {
        return totalStepsForMandW;
    }

    public void setTotalStepsForMandW(double totalStepsForMandW) {
        this.totalStepsForMandW = totalStepsForMandW;
    }

    private double totalStepsForMandW;



    public AggregateInformation() {

    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getTotalSteps() {
        return totalSteps;
    }

    public void setTotalSteps(int totalSteps) {
        this.totalSteps = totalSteps;
    }

    public int getCalsBurned() {
        return calsBurned;
    }

    public void setCalsBurned(int calsBurned) {
        this.calsBurned = calsBurned;
    }

    public int getHealthyPaceSteps() {
        return healthyPaceSteps;
    }

    public void setHealthyPaceSteps(int healthyPaceSteps) {
        this.healthyPaceSteps = healthyPaceSteps;
    }

    public int getHealthyPaceDUration() {
        return healthyPaceDUration;
    }

    public void setHealthyPaceDUration(int healthyPaceDUration) {
        this.healthyPaceDUration = healthyPaceDUration;
    }

    @Override
    public String toString() {
        return "AggregateInformation{" +
                "type='" + type + '\'' +
                ", date=" + date +
                ", totalSteps=" + totalSteps +
                ", calsBurned=" + calsBurned +
                ", healthyPaceSteps=" + healthyPaceSteps +
                ", healthyPaceDUration=" + healthyPaceDUration +
                ", distance=" + distance +
                '}';
    }
}
