package com.rawfit.me.stepcounter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.rawfit.me.main.Converter;
import com.rawfit.me.main.Dashboard;

import com.rawfit.me.main.R;
import com.rawfit.me.main.TabFragment2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rawfit.me.SQL.SQLiteHelper;
import com.squareup.leakcanary.LeakCanary;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.view.ColumnChartView;

import static com.rawfit.me.main.Dashboard.animate;
import static com.rawfit.me.main.Dashboard.initLeakCanary;
import static com.rawfit.me.main.Dashboard.user;

public class ScrollingActivity extends AppCompatActivity {

    private static final String TAG = "ScrollingActivity";

    //tv
    public TextView filterLabel;
    public TextView historicStepsLabel;
    public TextView historicCalsBurnedLabel;
    public TextView historicCalsLabel;
    public TextView historicHealthyPaceLabel;
    public TextView historicHealthyPaceTimeLabel;
    public TextView historicHealthyPaceTv;
    public TextView historicActiveDurationTv;
    public TextView historicalDistanceTv;
    public TextView historicalDistanceTvLabel;
    public TextView totalStepsTv;
    public TextView totalDistance;

    //sqlite db
    private SQLiteHelper db;

    //column chart
    private ColumnChartView chart;
    private List<SubcolumnValue> values;
    private ColumnChartData data;







    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLeakCanary(getApplication(), this);

        //TODO: consider using bundle instead
        AggregateInformation ai = (AggregateInformation) getIntent().getSerializableExtra("AggregateInformation");


        String unit = StepCounterActivity.user.getUnit();



        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);



        //animation
        animate(getWindow());

        if(ai.getType().equals("daily")) {
            toolbar.setTitle("Daily Summary: " + TabFragment2.getDateByDayNumber(ai.getDate(), 2017) + "/" + TabFragment2.YEAR);
        }
        if(ai.getType().equals("weekly")) {
            toolbar.setTitle("Weekly Summary: " + TabFragment2.getDatesBetweenWeekNumber(ai.getDate()));
        }
        if(ai.getType().equals("monthly")) {
            toolbar.setTitle("Monthly Summary: " + TabFragment2.MONTHS[ai.getDate()]);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

        //tv
        filterLabel=(TextView) findViewById(R.id.filterLabel);
        historicStepsLabel=(TextView) findViewById(R.id.historicStepsLabel);
        historicCalsBurnedLabel=(TextView) findViewById(R.id.historicCalsBurnedLabel);
        historicCalsLabel=(TextView) findViewById(R.id.historicCalsLabel);
        historicHealthyPaceLabel=(TextView) findViewById(R.id.historicHealthyPaceLabel);
        historicHealthyPaceTimeLabel=(TextView) findViewById(R.id.historicHealthyPaceTimeLabel);
        historicHealthyPaceTv=(TextView) findViewById(R.id.historicHealthyPaceTv);
        historicActiveDurationTv=(TextView) findViewById(R.id.historicActiveDurationTv);
        historicalDistanceTv=(TextView) findViewById(R.id.historicalDistanceTv);
        historicalDistanceTvLabel=(TextView) findViewById(R.id.historicalDistanceTvLabel);
        totalStepsTv=(TextView) findViewById(R.id.totalStepsTv);
        totalDistance=(TextView) findViewById(R.id.totalDistance);

        //chart
        chart = (ColumnChartView) findViewById(R.id.step24hrChart);

        int sec = ai.getHealthyPaceDUration();
        int totalSteps = ai.getHealthyPaceSteps();

        if(ai.getType().equals("daily")) {

            CardView cv = (CardView) findViewById(R.id.card_view3);
            cv.setVisibility(View.GONE);

            totalStepsTv.setVisibility(View.INVISIBLE);
            totalDistance.setVisibility(View.INVISIBLE);

            filterLabel.setText("Total Daily Steps:");
            historicStepsLabel.setText("" + ai.getTotalSteps());
            historicCalsLabel.setText("Daily Total Calories Burned:");
            historicCalsBurnedLabel.setText("" + ai.getCalsBurned());
            historicHealthyPaceLabel.setText("Daily Total Healthy Pace:");
            historicalDistanceTv.setText("Daily Total Distance Walked:");

            if(unit.equals("metric")){
                historicalDistanceTvLabel.setText(ai.getDistance()+" km");
            }
            if(unit.equals("imperial")) {
                historicalDistanceTvLabel.setText(ai.getDistance() * (0.62137) + " mi");
            }

            historicHealthyPaceLabel.setText("" + totalSteps + " steps");

            historicHealthyPaceTimeLabel.setText(""+ Converter.secondsToHrsMinSec(sec));


            new DoInBg(ai.getDate()+1, getApplicationContext()).execute();//TODO: check
        }
        if(ai.getType().equals("weekly")) {
            //chart.setVisibility(View.INVISIBLE);
            filterLabel.setText("Daily Average Steps:");
            historicStepsLabel.setText("" + ai.getTotalSteps());
            historicCalsLabel.setText("Daily Average Calories Burned:");
            historicCalsBurnedLabel.setText("" + ai.getCalsBurned());
            historicHealthyPaceLabel.setText(""+ai.getHealthyPaceSteps() + " steps");

            historicHealthyPaceTimeLabel.setText(""+ Converter.secondsToHrsMinSec(sec));


            historicHealthyPaceTv.setText("Daily Average Healthy Pace:");
            historicActiveDurationTv.setText("Daily Average Active Time:");
            historicalDistanceTv.setText("Daily Average Distance Walked:");

            if(unit.equals("metric")){
                historicalDistanceTvLabel.setText(ai.getDistance()+" km");
            }
            if(unit.equals("imperial")){
                historicalDistanceTvLabel.setText(ai.getDistance()*(0.62137)+" mi");
            }

            //totalCOunt
            totalStepsTv.setText("Total steps: " + (int)ai.getTotalStepsForMandW());

            if(unit.equals("metric")){
                totalDistance.setText("Total distance: " +(user.getStrideLength()*ai.getTotalStepsForMandW())/1000+" km");
            }
            if(unit.equals("imperial")) {
                totalDistance.setText("Total distance: " + (user.getStrideLength()*ai.getTotalStepsForMandW())/1000 * (0.62137) + " mi");
            }


            new DoInBg2(ai.getDate(), getApplicationContext()).execute();//TODO: check if weeknumber corresponds with days in that week in the cart
        }
        if(ai.getType().equals("monthly")) {

            //gone wont use space in the layout
            CardView cv = (CardView) findViewById(R.id.card_view);
            cv.setVisibility(View.GONE);


            chart.setVisibility(View.INVISIBLE);
            filterLabel.setText("Daily Average Steps:");
            historicStepsLabel.setText("" + ai.getTotalSteps());
            historicCalsLabel.setText("Daily Average Calories Burned:");
            historicCalsBurnedLabel.setText("" + ai.getCalsBurned());
            historicHealthyPaceLabel.setText(""+ai.getHealthyPaceSteps() + " steps");
            historicHealthyPaceTimeLabel.setText(""+ Converter.secondsToHrsMinSec(sec));
            historicHealthyPaceTv.setText("Daily Average Healthy Pace:");
            historicActiveDurationTv.setText("Daily Average Active Time:");
            historicalDistanceTv.setText("Daily Average Distance Walked:");

            if(unit.equals("metric")){
                historicalDistanceTvLabel.setText(ai.getDistance()+" km");
            }
            if(unit.equals("imperial")){
                historicalDistanceTvLabel.setText(ai.getDistance()*(0.62137)+" mi");
            }

            //totalCOunt
            totalStepsTv.setText("Total steps: " + (int)ai.getTotalStepsForMandW());
            if(unit.equals("metric")){
                totalDistance.setText("Total distance: " +(user.getStrideLength()*ai.getTotalStepsForMandW())/1000+" km");
            }
            if(unit.equals("imperial")) {
                totalDistance.setText("Total distance: " + (user.getStrideLength()*ai.getTotalStepsForMandW())/1000 * (0.62137) + " mi");
            }
        }

    }


    @Override
    public void onDestroy(){
        super.onDestroy();

        //Log.d(TAG,"onDestroy()");
    }

    //for the daily total per week
    private class DoInBg2 extends AsyncTask<String, Integer, Cursor> {

        private int weekNumber;
        private Context context;

        public DoInBg2(int weekNumber, Context context){
            this.weekNumber = weekNumber;
            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            //pb.setVisibility(View.VISIBLE);
            //Log.d(TAG," Task Starting...");
        }


        @Override
        protected Cursor doInBackground(String... params) {

            db = SQLiteHelper.getSqLiteHelper(context);

            return db.getTotalDailyStepForWeekNumber(weekNumber,TabFragment2.YEAR);

        }


        @Override
        protected void onProgressUpdate(Integer... params) {
            //pb.setProgress(params[0]);
            //Log.d(TAG,params[0] + " progress");
            //pb.setIndeterminate(true);

        }

        @Override
        protected void onPostExecute(Cursor result) {


            //hour count list
            //List<AggregationValue> aggregationValues = new ArrayList<>();
            final AggregationValue[] aggregationValues = new AggregationValue[7];

            for(int i = 0; i<7; i++) {

                    aggregationValues[i]=new AggregationValue();
                    aggregationValues[i].setDate(i);
                    aggregationValues[i].setValue(0);
                    aggregationValues[i].setExtra(0);
            }


            result.moveToFirst();
            while (result.isAfterLast() == false) {
                if(result.getString(0)!=null) {
                    int day = Integer.parseInt(result.getString(1));

                    int dailyCount = Integer.parseInt(result.getString(0));

                    int dayOfYear = Integer.parseInt(result.getString(4));



                    for(int i = 0; i<7 ;i++) {

                        if(aggregationValues[i].getDate() == day) {
                            aggregationValues[i].setValue(dailyCount);
                            aggregationValues[i].setExtra(dayOfYear);
                        }

                    }

                    //Log.d(TAG, " day from db " + day + " " + dailyCount+ " " + result.getString(2) + " " + result.getString(4));
                }
                result.moveToNext();
            }
            result.close();




            //column chart
            //chart = (ColumnChartView) getActivity().findViewById(R.id.step24hrChart);
            List<AxisValue> axisValues = new ArrayList<>();
            List<AxisValue> axisValuesTop = new ArrayList<>();
            List<Column> columns = new ArrayList<>();
            //List<SubcolumnValue> values = null;


            //String[] days = new String[]{"Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
            String[] days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};

            for (int i = 0; i < 7; ++i) {
                values = new ArrayList<>();

                if (i == 0) {
                    values.add(new SubcolumnValue((float) (aggregationValues[1].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[0]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[1].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[1]);
                } else if (i == 1) {
                    values.add(new SubcolumnValue((float) (aggregationValues[2].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[1]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[2].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[1]);
                } else if (i == 2) {
                    values.add(new SubcolumnValue((float) (aggregationValues[3].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[2]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[3].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[2]);
                } else if (i == 3) {
                    values.add(new SubcolumnValue((float) (aggregationValues[4].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[3]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[4].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[3]);
                } else if (i == 4) {
                    values.add(new SubcolumnValue((float) (aggregationValues[5].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[4]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[5].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[4]);
                } else if (i == 5) {
                    values.add(new SubcolumnValue((float) (aggregationValues[6].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[5]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[6].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[5]);
                } else if (i == 6) {
                    values.add(new SubcolumnValue((float) (aggregationValues[0].getValue())).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(6).setLabel(days[6]));
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[0].getValue()+""));
                    columns.add(new Column(values).setHasLabels(false));
                    //Log.d(TAG, i + " " + days[6]);
                } else {
                    values.add(new SubcolumnValue(0).setColor(Dashboard.colColor));

                    axisValues.add(new AxisValue(i).setLabel(days[i - 1]));
                    columns.add(new Column(values).setHasLabels(false));
                }
            }

            data = new ColumnChartData(columns);

            data.setAxisXBottom(new Axis(axisValues).setHasLines(false).setName("Days").setTextColor(Dashboard.colColor));
            data.setAxisXTop(new Axis(axisValuesTop).setHasLines(false).setName("Steps").setTextColor(Dashboard.colColor));

            chart.setZoomType(ZoomType.HORIZONTAL);


            chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int i, int i1, SubcolumnValue subcolumnValue) {
                    //Log.d(TAG, " on val selected " + i);

                    int dayOfYear = 0;

                                         if(i==0) {
                        dayOfYear  = (int) aggregationValues[1].getExtra();
                    }                    if(i==1) {
                        dayOfYear = (int) aggregationValues[2].getExtra();
                    }                    if(i==2) {
                        dayOfYear  = (int) aggregationValues[3].getExtra();
                    }                    if(i==3) {
                        dayOfYear  = (int) aggregationValues[4].getExtra();
                    }                    if(i==4) {
                        dayOfYear  = (int) aggregationValues[5].getExtra();
                    }                    if(i==5) {
                        dayOfYear  = (int) aggregationValues[6].getExtra();
                    }                    if(i==6) {
                        dayOfYear  = (int) aggregationValues[0].getExtra();
                    }

                            int sec = 0;
                            int totalSteps = 0;
                    //Log.d(TAG, " on val selected " + dayOfYear);
                            Cursor cursor = db.getHealthyPaceByDate(dayOfYear, TabFragment2.YEAR);
                            cursor.moveToFirst();
                    //Log.d(TAG, sec + " " + totalSteps + " i is " + dayOfYear);
                            while (cursor.isAfterLast() == false) {
                                try {

                                    sec = Integer.parseInt(cursor.getString(0).toString());
                                    totalSteps = Integer.parseInt(cursor.getString(1));
                                    //Log.d(TAG, sec + " " + totalSteps + " i is " + i);
                                }
                                catch (Exception e) {
                                    //Log.d(TAG, e.toString() + " on val selected");
                                }

                                cursor.moveToNext();

                            }
                            cursor.close();




                            //on value touch pass new object to new activity and show more information there
                            AggregateInformation ai = new AggregateInformation();
                            ai.setType("daily");
                            ai.setTotalSteps((int)subcolumnValue.getValue());
                            ai.setDate(dayOfYear-1);
                            ai.setHealthyPaceSteps(totalSteps);
                            ai.setHealthyPaceDUration(sec);
                            ai.setCalsBurned(StepCounterActivity.calsPerStep(StepCounterActivity.user, (double)subcolumnValue.getValue()));
                            ai.setDistance((StepCounterActivity.user.getStrideLength()*ai.getTotalSteps())/1000);

                            Intent intent = new Intent(getApplicationContext(), ScrollingActivity.class);
                            intent.putExtra("AggregateInformation", (Serializable) ai);
                            startActivity(intent);





                    //Log.d(TAG, " on touch day " + i + " " + aggregationValues[i].getExtra());
                    Toast.makeText(context,subcolumnValue.getValue()+"", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onValueDeselected() {

                }
            });


            chart.setColumnChartData(data);



            //pb.setIndeterminate(false);
            //pb.setVisibility(View.INVISIBLE);
            try {// TODO: causes error when esper query fires on new day find fix later
                //Toast.makeText(getContext(), "Done", Toast.LENGTH_SHORT).show();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    //for the hourly total per day
    private class DoInBg extends AsyncTask<String, Integer, Cursor> {

        private int dayNumber;
        private Context context;

        public DoInBg(int dayNumber, Context context){
            this.dayNumber=dayNumber;
            this.context=context;
        }

        @Override
        protected void onPreExecute() {
            //pb.setVisibility(View.VISIBLE);
            //Log.d(TAG," Task Starting...");
        }


        @Override
        protected Cursor doInBackground(String... params) {

            db = SQLiteHelper.getSqLiteHelper(context);

            return db.getTotalStepsPrHourByDate(dayNumber,TabFragment2.YEAR);

        }


        @Override
        protected void onProgressUpdate(Integer... params) {
            //pb.setProgress(params[0]);
            //Log.d(TAG,params[0] + " progress");
            //pb.setIndeterminate(true);

        }

        @Override
        protected void onPostExecute(Cursor result) {


            //hour count list
            //List<AggregationValue> aggregationValues = new ArrayList<>();
            AggregationValue[] aggregationValues = new AggregationValue[24];

            for(int i =0;i<24;i++) {
                //aggregationValues.add(i, new AggregationValue());
                aggregationValues[i]=new AggregationValue();
                aggregationValues[i].setDate(i);
                aggregationValues[i].setValue(0);
            }


            result.moveToFirst();
            while (result.isAfterLast() == false) {
                if(result.getString(0)!=null) {
                    int hour = Integer.parseInt(result.getString(1));
                    int hourlyCount = Integer.parseInt(result.getString(0));
                    //aggregationValues.get(hour).setDate(hour);
                    //aggregationValues.get(hour).setValue(hourlyCount);
                    //aggregationValues[hour].setDate(hour);
                    //aggregationValues[hour].setValue(hourlyCount);

                    for(int i =0;i<24;i++) {

                        if(aggregationValues[i].getDate()==hour){
                            aggregationValues[i].setValue(hourlyCount);
                        }

                    }

                    //Log.d(TAG, " hour from db " + hour + " " + hourlyCount);
                }
                result.moveToNext();
            }
            result.close();

            //column chart
            //chart = (ColumnChartView) getActivity().findViewById(R.id.step24hrChart);
            List<AxisValue> axisValues = new ArrayList<>();
            List<Column> columns = new ArrayList<>();
            //List<SubcolumnValue> values = null;




            for (int i = 0; i < 24; ++i) {
                values = new ArrayList<>();

                //adds the current hour count to the column value for the current hour
                if(aggregationValues[i].getDate()== new Date().getHours()) {
                    values.add(new SubcolumnValue((float) (aggregationValues[i].getValue()/*+currentHourlyCount*/)).setColor(Dashboard.colColor));
                }
                else if(aggregationValues[i].getDate()==i){
                    values.add(new SubcolumnValue((float) (aggregationValues[i].getValue())).setColor(Dashboard.colColor));
                    //Log.d(TAG, " getDateCheck1 " + i);
                }
                else {
                    values.add(new SubcolumnValue(0).setColor(Dashboard.colColor));
                    //Log.d(TAG, " getDateCheck3 " + i);
                }

                axisValues.add(new AxisValue(i).setLabel(i+""));
                columns.add(new Column(values).setHasLabels(false));
            }

            data = new ColumnChartData(columns);

            data.setAxisXBottom(new Axis(axisValues).setHasLines(false).setName("Hour").setTextColor(Dashboard.colColor));
            data.setAxisYLeft(new Axis().setHasLines(false).setName("Steps").setTextColor(Dashboard.colColor));

            chart.setZoomType(ZoomType.HORIZONTAL);


            chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int i, int i1, SubcolumnValue subcolumnValue) {
                    Toast.makeText(context,subcolumnValue.getValue()+"", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onValueDeselected() {

                }
            });


            chart.setColumnChartData(data);



            //pb.setIndeterminate(false);
            //pb.setVisibility(View.INVISIBLE);
            try {// TODO: causes error when esper query fires on new day find fix later
                //Toast.makeText(getContext(), "Done", Toast.LENGTH_SHORT).show();
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }

}
