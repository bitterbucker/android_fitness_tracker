package com.rawfit.me.SQL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.rawfit.me.stepcounter.Achievement;
import com.rawfit.me.main.User;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;


public class SQLiteHelper extends SQLiteOpenHelper{

    private static final String TAG = "SQL";

    private static SQLiteHelper sqLiteHelper;

    private static final int DB_VERSION = 1;

    public static int counter=0;

    //singelton pattern
    public static synchronized  SQLiteHelper getSqLiteHelper(Context context) {
        if (sqLiteHelper == null) {
            sqLiteHelper = new SQLiteHelper(context.getApplicationContext());
        counter++;
        }
        return sqLiteHelper;
    }

    //construtor
    private SQLiteHelper(Context context) {
        super(context, "stepDb" , null, DB_VERSION);
    }

    /*public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }*/

    //TODO: remove
    public void truncateStepTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(
"delete from steps"
        );
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(
"create table steps (id integer primary key AUTOINCREMENT, timestamp long, step double)"
        );

        sqLiteDatabase.execSQL(
"create table calories (id integer primary key AUTOINCREMENT, timestamp long, calorie double)"
        );

        sqLiteDatabase.execSQL(
"create table weight (id integer primary key AUTOINCREMENT, timestamp long, weight double)"
        );

        sqLiteDatabase.execSQL(
 "CREATE TABLE `user` (`id` INTEGER NOT NULL PRIMARY KEY, `weight` DOUBLE,`height` DOUBLE,`stepGoal` INTEGER,`calGoal` INTEGER, `unit` TEXT, `gender` TEXT,  `stride` DOUBLE, `age` INTEGER, `startAtBoot` INTEGER, `stepSensorType` INTEGER)"
        );

        sqLiteDatabase.execSQL(
"CREATE TABLE `healthyPace` (`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`timestamp` long NOT NULL,`duration` long NOT NULL,`stepCount` INTEGER)"
        );

        //table for all achievements
        sqLiteDatabase.execSQL(
"CREATE TABLE `achievement`(`id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,`achievement` TEXT NOT NULL,`timestamp` long NOT NULL,`currentGoalWhenAchieved` INTEGER)"
        );

        //init user
        sqLiteDatabase.execSQL(
"insert into user (weight,height,stepGoal,calGoal,unit,gender, stride, age, startAtBoot, stepSensorType) values(0,0,0,0,'metric','U', 0, 0, 0, 0)"
        );

    }

    //insert achieveement, should only have one achievement per dayf ro a specific type i.e stepgoal
    public void insertAchievement(String achievement, long timestamp, int currentGoalWhenAchieved) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("achievement", achievement);
        contentValues.put("timestamp", timestamp);
        contentValues.put("currentGoalWhenAchieved", currentGoalWhenAchieved);

        db.insert("achievement", null, contentValues);
    }

    public int getAchievement(int day, int year){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select count(*), achievement, currentGoalWhenAchieved from achievement where strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '" + day + "-" + year + "' and achievement = 'DSG'", null);

        cursor.moveToFirst();
        int achievementCount = 0;
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                achievementCount = Integer.parseInt(cursor.getString(0).toString());
                //Log.d(TAG, cursor.getString(0) + " " + cursor.getString(1) + " " + cursor.getString(2) + " achiv by day and YEAR " + day + " " + year);
            }
            cursor.moveToNext();
        }
        cursor.close();

        return achievementCount;
    }

    public List<Achievement> getAchievement(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select achievement, currentGoalWhenAchieved, timestamp from achievement", null);

        List<Achievement> achievementList = new ArrayList<>();
        int i=0;
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                //Log.d(TAG, cursor.getString(0) + cursor.getString(1) + cursor.getString(2) + " all achivment ");
                achievementList.add(i, new Achievement(cursor.getString(0).toString(), Integer.parseInt(cursor.getString(1).toString()), Long.parseLong(cursor.getString(2).toString())));
            }
            cursor.moveToNext();
            i++;
        }
        cursor.close();

        return achievementList;
    }

    public double getTotalStepCountOfAllTime() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
 "select sum(step) from steps", null);

        double totalSteps = 0;
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {
            if (cursor.getString(0) != null) {
                //Log.d(TAG, cursor.getString(0));
                totalSteps = Double.parseDouble(cursor.getString(0));
            }
            cursor.moveToNext();
        }
        cursor.close();

        return totalSteps;
    }

    public Cursor getAverageHealthyPacePerMonth(int month, int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select avg(d.sumStepCount),   avg(d.sumDuration)from(select timestamp as dtt, strftime('%d-%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') as dmy ,strftime('%m', timestamp / 1000, 'unixepoch', 'localtime') as m,strftime('%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') as my, count(*) as ca , sum(duration) as sumDuration , sum(stepCount) as sumStepCount from healthyPace where strftime('%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '" + month + "-" + year + "' group by strftime('%d-%m-%Y', timestamp / 1000, 'unixepoch', 'localtime')) as d group by strftime('%m-%Y', d.dtt / 1000, 'unixepoch', 'localtime')", null);

        return cursor;
    }

    public Cursor getAverageHealthyPacePerWeek(int month, int year) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(
"select avg(d.sumStepCount),   avg(d.sumDuration) from(select timestamp as dtt, strftime('%W-%Y', timestamp / 1000, 'unixepoch', 'localtime') as dmy ,strftime('%W', timestamp / 1000, 'unixepoch', 'localtime') as m,strftime('%W-%Y', timestamp / 1000, 'unixepoch', 'localtime') as my, count(*) as ca , sum(duration) as sumDuration , sum(stepCount) as sumStepCount from healthyPace where strftime('%W-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '" + month + "-" + year + "' group by strftime('%d-%W-%Y', timestamp / 1000, 'unixepoch', 'localtime')) as d group by strftime('%W-%Y', d.dtt / 1000, 'unixepoch', 'localtime')", null);

        return cursor;
    }


    //TODO: implement functionality for this later
    public Cursor getMaxStepOfAllTime(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select max(c) from (select sum(step) as c ,strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') as jy from steps group by jy) d",null);

        return cursor;
    }

    public void insertHealthyPaceSession(long seconds, long timestamp, int hpCount) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("timestamp", timestamp);
        contentValues.put("duration", seconds);
        contentValues.put("stepCount", hpCount);
        db.insert("healthyPace", null, contentValues);
    }


    public Cursor getHealthyPaceByDate(int day, int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select sum(duration) as sumSeconds, sum(stepCount) as sumStepCount from healthyPace where strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+day+"-"+year+"' ", null);

        return cursor;
    }

    public void insertStep(long timestamp, double step) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("timestamp", timestamp);
        contentValues.put("step", step);
        db.insert("steps", null, contentValues);
    }

    //TODO: check if offset is needed
    public Cursor getHealthyPace() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select sum(duration) as sumSeconds, sum(stepCount) as sumStepCount from healthyPace where date(datetime(timestamp / 1000 , 'unixepoch', 'localtime')) = date('now','localtime') ", null);

        return cursor;
    }

    //TODO: check if offset is needed
    public Cursor getTotalSteps() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(
"select sum(step) as sumSteps from steps where date(datetime(timestamp / 1000 , 'unixepoch', 'localtime')) = date('now','localtime') ", null);

        return cursor;
    }

    //TODO: check if offset is needed
    public Cursor getTotalStepsPrHour() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select sum(step),strftime('%H', timestamp / 1000, 'unixepoch', 'localtime') as hrs from steps WHERE date(datetime(timestamp / 1000 , 'unixepoch', 'localtime')) = date('now','localtime') GROUP BY strftime('%H', timestamp / 1000, 'unixepoch', 'localtime') order by strftime('%H', timestamp / 1000, 'unixepoch', 'localtime') asc", null );

        return cursor;
    }

    public Cursor getTotalStepsPrHourByDate(int daynumber, int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select sum(step),strftime('%H', timestamp / 1000, 'unixepoch', 'localtime') as hrs from steps WHERE strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+daynumber+"-"+year+"' GROUP BY strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') order by strftime('%H', timestamp / 1000, 'unixepoch', 'localtime') asc", null );

        return cursor;
    }


    public Cursor getTotalDailyStepForWeekNumber(int weekNumber, int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select sum(step),strftime('%w', timestamp / 1000, 'unixepoch', 'localtime') as wk,strftime('%d-%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') as date,strftime('%w', timestamp / 1000, 'unixepoch', 'localtime') as wk,strftime('%j', timestamp / 1000, 'unixepoch', 'localtime') as d from steps WHERE strftime('%W-%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+weekNumber+"-"+year+"' GROUP BY strftime('%W-%w-%Y', timestamp / 1000, 'unixepoch', 'localtime') order by strftime('%d-%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') asc", null );

        return cursor;
    }


    public Cursor getAverageStepsPerMonth(int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select avg(d.dayilyCount), strftime('%m', d.dt / 1000, 'unixepoch', 'localtime') as m, sum(d.dayilyCount)  from (select sum(step) as dayilyCount, timestamp as dt, strftime('%d-%m-%Y', timestamp / 1000, 'unixepoch', 'localtime') as wmy from steps where strftime('%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+year+"' group by wmy) d group by strftime('%m-%Y', d.dt / 1000, 'unixepoch', 'localtime')", null );

        return cursor;
    }

    public Cursor getAverageStepsPerWeek(int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select avg(d.dayilyCount), strftime('%W', d.dt / 1000, 'unixepoch', 'localtime'), d.dwmy, strftime('%W-%m-%Y', d.dt / 1000, 'unixepoch', 'localtime'), sum(d.dayilyCount)  from( select sum(step) as dayilyCount, timestamp as dt, strftime('%d-%W-%Y', timestamp / 1000, 'unixepoch', 'localtime') as dwmy from steps where strftime('%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+year+"' group by dwmy ) d group by strftime('%W-%Y', d.dt / 1000, 'unixepoch', 'localtime')", null );//removed %m or month shift will result in fail

        return cursor;
    }

    public Cursor getTotalStepsPerDay(int year) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery(
"select sum(step), strftime('%j', timestamp / 1000, 'unixepoch', 'localtime') as d from steps where strftime('%Y', timestamp / 1000, 'unixepoch', 'localtime') = '"+year+"' group by strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') ", null );

        return cursor;
    }






//--------------------

    public int currentHourCount(long timestamp){
        SQLiteDatabase db = this.getReadableDatabase();

        int count=0;
        Cursor cursor= db.rawQuery(
 "select timestamp, step,strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') from steps WHERE strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = strftime('%H-%j-%Y', "+timestamp+" / 1000, 'unixepoch', 'localtime')", null);
        cursor.moveToFirst();


        int currentHourCount = 0;
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                //Log.d(TAG, cursor.getString(0) + " " + cursor.getString(1)+ " " + cursor.getString(2));
                currentHourCount=Integer.parseInt(cursor.getString(1).toString());
            }
            cursor.moveToNext();
            count++;

        }
        cursor.close();

        return currentHourCount;
    }

    public void updateCurrentHour(int currentHourStepCount, long timestamp){
        SQLiteDatabase db = this.getReadableDatabase();
        //String q1 = " UPDATE steps SET step = "+currentHourStepCount+", timestamp = "+timestamp+" WHERE strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = strftime('%H-%j-%Y', "+timestamp+" / 1000, 'unixepoch', 'localtime'); ";

        //String q2 = " INSERT INTO steps(step,timestamp) SELECT "+currentHourStepCount+" , "+timestamp+" WHERE (Select Changes() = 0); ";

        //StringBuilder sb = new StringBuilder();
        //sb.append(q1);
        //sb.append(q2);

        int count=0;
        Cursor cursor= db.rawQuery(
                "select timestamp, step,strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') from steps WHERE strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = strftime('%H-%j-%Y', "+timestamp+" / 1000, 'unixepoch', 'localtime')", null);
        cursor.moveToFirst();


        int currentHourCount = 0;
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                //Log.d(TAG, cursor.getString(0) + " " + cursor.getString(1)+ " " + cursor.getString(2));
                currentHourCount=Integer.parseInt(cursor.getString(1).toString());
            }
            cursor.moveToNext();
            count++;

        }
        cursor.close();

        if(count>0){
            updateHour(currentHourStepCount,timestamp);
            //Log.d(TAG, "updating");
        }
        else{
            insertNewHour(currentHourStepCount,timestamp);
            //Log.d(TAG, "inserting");
        }

    }

    //update current hour
    public void updateHour(int currentHourStepCount, long timestamp){
        SQLiteDatabase db = this.getWritableDatabase();
        String q1 = " UPDATE steps SET step = "+currentHourStepCount+", timestamp = "+timestamp+" WHERE strftime('%H-%j-%Y', timestamp / 1000, 'unixepoch', 'localtime') = strftime('%H-%j-%Y', "+timestamp+" / 1000, 'unixepoch', 'localtime'); ";

        db.execSQL(q1);
        //db.close();
    }

public void insertNewHour(int currentHourStepCount, long timestamp) {
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues contentValues = new ContentValues();
    contentValues.put("step", currentHourStepCount);
    contentValues.put("timestamp", timestamp);
    db.insert("steps", null, contentValues);
    //db.close()
}




//---------------------------------





//---------------------------------calories


    public void insertCalorie(int calorie, long timestamp){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("calorie", calorie);
        contentValues.put("timestamp", timestamp);
        db.insert("calories", null, contentValues);
        //db.close()
    }





    //---------------------------------weight

    public void insertWeight(int weight, long timestamp){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("weight", weight);
        contentValues.put("timestamp", timestamp);
        db.insert("weight", null, contentValues);
        //db.close()
    }










    //--------------------------------- user
    public void updateUserData(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("weight", user.getWeight());
        contentValues.put("height", user.getHeight());
        contentValues.put("stepGoal", user.getStepGoal());
        contentValues.put("calGoal", user.getCalorieBurnedPerDayGoal());
        contentValues.put("unit", user.getUnit());
        contentValues.put("gender", user.getGender().getval());
        contentValues.put("age", user.getAge());
        contentValues.put("startAtBoot", user.getStartAtBoot());
        contentValues.put("stepSensorType", user.getStepSensorType());

        db.update("user", contentValues, "id=?", new String[]{String.valueOf(1)});
    }

    public void insertUserData(String name, int weight, int height, int stepGoal, int calGoal, String unit, String g, double stride, int age, int startAtBoot, int stepSensorType) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put("weight", weight);
        contentValues.put("height", height);
        contentValues.put("stepGoal", stepGoal);
        contentValues.put("calGoal", calGoal);
        contentValues.put("unit", unit);
        contentValues.put("gender", g);
        contentValues.put("stride", stride);
        contentValues.put("age", age);
        contentValues.put("startAtBoot", startAtBoot);
        contentValues.put("stepSensorType", stepSensorType);
        db.insert("user", null, contentValues);
    }

    public User getUser(){
        User user = new User();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor =  db.rawQuery("select id, weight, height, stepGoal, calGoal, unit, gender, stride, age, startAtBoot, stepSensorType from user where id = 1", null );

        cursor.moveToFirst();
        int c=0;
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                user.setName("TEST");
                user.setWeight(Double.parseDouble(cursor.getString(1)));
                user.setHeight(Double.parseDouble(cursor.getString(2)));
                user.setStepGoal(Integer.parseInt(cursor.getString(3)));
                user.setCalorieBurnedPerDayGoal(Integer.parseInt(cursor.getString(4)));
                user.setUnit(cursor.getString(5).toString());



                if(cursor.getString(6).toString().equals("M")){
                    user.setGender(User.Gender.MALE);
                    //Log.d(TAG, cursor.getString(6).toString());
                }
                else if(cursor.getString(6).toString().equals("F")) {
                    user.setGender(User.Gender.FEMALE);
                    //Log.d(TAG, cursor.getString(6).toString());
                }
                else{
                    user.setGender(User.Gender.UNKNOWN);
                    //Log.d(TAG, cursor.getString(6).toString());
                }


                user.setStride(Double.parseDouble(cursor.getString(7)));
                user.setAge(Integer.parseInt(cursor.getString(8)));
                user.setStartAtBoot(Integer.parseInt(cursor.getString(9)));
                user.setStepSensorType(Integer.parseInt(cursor.getString(10)));
                //Log.d(TAG, cursor.getString(8).toString() + " yrs");
            }
            cursor.moveToNext();
            c++;
        }
        cursor.close();

        return user;
    }



    //---------------------------------

    public void countRows(){ //count(*) should be 24 per day/ group
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor= db.rawQuery(
                "select count(*) from steps group by strftime('%j-%Y', timestamp / 1000, 'unixepoch', 'localtime')", null);
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                //Log.d("TAG", cursor.getString(0) + " row count");
            }
            cursor.moveToNext();
        }
        cursor.close();

    }


    //random data generator-------------------------
    private String getDateByDayNumber(int dayNumber, int year) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DATE, 1);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, dayNumber);
        Date date = c.getTime();

        return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public void genRandomHourlyData() {
        Calendar doy = Calendar.getInstance();

        for (int i = 0; i < doy.get(Calendar.DAY_OF_YEAR) - 1; i++) {

            String df = getDateByDayNumber(i, 2017);

            for (int day = 0; day < 24; day++) {

                long offset2;
                if (day < 10) {
                    offset2 = Timestamp.valueOf(df + " 0" + day + ":00:00").getTime();
                } else {
                    offset2 = Timestamp.valueOf(df + " " + day + ":00:00").getTime();
                }

                //System.out.println(new Date(offset2));
                insertStep(offset2, new Random().nextInt(500));
            }
        }
    }
    //----------------------random data gen end-------------------------------------


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        SQLiteDatabase db = this.getWritableDatabase();

        /*db.execSQL("DROP TABLE IF EXISTS steps");
        db.execSQL("DROP TABLE IF EXISTS healthyPace");
        db.execSQL("DROP TABLE IF EXISTS user");

        Log.d(TAG, "DB upgraded");

        this.onCreate(db);*/
    }



}
