package com.rawfit.me.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.Toast;

import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.main.BackgroundService;


public class BootCompletedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent serviceIntent = new Intent(context, BackgroundService.class);

        SQLiteHelper db = SQLiteHelper.getSqLiteHelper(context);
        //query for startatboot option. if true start service at boot and if false dont do anything unless user decides to change settings
        Cursor cursor= db.getReadableDatabase().rawQuery("select startAtBoot from user", null);
        cursor.moveToFirst();
        boolean startAtBoot=false;

        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                if(Integer.parseInt(cursor.getString(0))==0){
                    startAtBoot=false;
                }
                if(Integer.parseInt(cursor.getString(0))==1){
                    startAtBoot=true;
                }
            }
            cursor.moveToNext();
        }
        cursor.close();


        if(startAtBoot==true){
            context.startService(serviceIntent);
            Toast.makeText(context, "RawFit Service Started at boot", Toast.LENGTH_LONG).show();
        }


    }
}