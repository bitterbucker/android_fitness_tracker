package com.rawfit.me.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.util.Log;


public class StepSensorChangedReceiver extends BroadcastReceiver {

    private static final String TAG = "StepSensorChangedRecvr";

    private SensorManager sensorManager;
    private SensorEventListener sensorEventListener;

    public StepSensorChangedReceiver(SensorManager sensorManager, SensorEventListener sensorEventListener){
        this.sensorManager=sensorManager;
        this.sensorEventListener=sensorEventListener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        //Log.d(TAG, new Date() + "onReceive("+intent+")");

        if (!intent.getAction().equals("stepSensorChanged")) {
            return;
        }

        Runnable runnable = new Runnable() {
            public void run() {
                //Log.i(TAG, "Runnable executing. 2");


                int stepSensorType = intent.getIntExtra("value", -1);
                sensorManager.unregisterListener(sensorEventListener);


                //0 is default in this situation 1 is experimental, i.e, accelerometer with esper
                if(stepSensorType==0) {
                    if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_FASTEST);
                        //Log.d(TAG, "using TYPE_STEP_DETECTOR since user picked this option");
                    } else if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                        //Log.d(TAG, "using TYPE_ACCELEROMETER since TYPE_STEP_DETECTOR not supported");
                        if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                            sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                            //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                        }
                    }
                }
                else if(stepSensorType==1){
                    if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                        //Log.d(TAG, "using TYPE_ACCELEROMETER since user picked this option");
                        if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                            sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                            //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                        }
                    }
                }

            }
        };

        new Handler().postDelayed(runnable, 1000);
    }
}
