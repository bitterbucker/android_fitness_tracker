package com.rawfit.me.broadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

import com.rawfit.me.SQL.SQLiteHelper;

public class ScreenOffReceiver extends BroadcastReceiver {

    private static final String TAG = "ScreenOffReceiver";

    private SensorManager sensorManager;
    private SensorEventListener sensorEventListener;

    public ScreenOffReceiver(SensorManager sensorManager,SensorEventListener sensorEventListener){
        this.sensorManager=sensorManager;
        this.sensorEventListener=sensorEventListener;
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        //Log.d(TAG, new Date() + "onReceive("+intent+")");

        if (!intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            return;
        }

        Runnable runnable = new Runnable() {
            public void run() {
                //Log.d(TAG, "Runnable executing.");

                SQLiteHelper db = SQLiteHelper.getSqLiteHelper(context);
                //query for startatboot option. if true start service at boot and if false dont do anything unless user decides to change settings
                Cursor cursor = db.getReadableDatabase().rawQuery("select stepSensorType from user", null);
                cursor.moveToFirst();

                int stepSensorType = -1;

                while (cursor.isAfterLast() == false) {
                    if (cursor.getString(0) != null) {
                        stepSensorType = Integer.parseInt(cursor.getString(0));
                    }
                    cursor.moveToNext();
                }
                cursor.close();


                sensorManager.unregisterListener(sensorEventListener);


                /*//TODO: remember to change this also when switching sensortypes
                //0 is default in this situation 1 is experimental, i.e, accelerometer with esper
                if (stepSensorType == 0) {
                    sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_FASTEST);
                } else if (stepSensorType == 1) {
                    sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                    sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                }*/

                //0 is default in this situation 1 is experimental, i.e, accelerometer with esper
                if(stepSensorType==0){
                    if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_FASTEST);
                        //Log.d(TAG, "using TYPE_STEP_DETECTOR since user picked this option");
                    }
                    else{
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                        //Log.d(TAG, "using TYPE_ACCELEROMETER since TYPE_STEP_DETECTOR not supported");
                        if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
                            sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                            //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                        }
                    }
                }else if(stepSensorType==1) {
                    sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                    //Log.d(TAG, "using TYPE_ACCELEROMETER since user picked this option");
                    if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                        //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                    }
                }




            }
        };

        new Handler().postDelayed(runnable, 1000);
    }
}
