package com.rawfit.me.events;



public class AccelerometerSensor extends SensorEvent{

    private float x;
    private float y;
    private float z;

    public AccelerometerSensor(String sensorType, long timestamp, float[] values) {
        super(sensorType, timestamp, values);
        this.x = values[0];
        this.y = values[1];
        this.z = values[2];
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

}
