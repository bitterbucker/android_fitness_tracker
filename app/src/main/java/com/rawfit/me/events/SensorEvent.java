package com.rawfit.me.events;



public class SensorEvent {

    private String sensorType;
    private long timestamp;
    private float values[];

    public SensorEvent(String sensorType, long timestamp, float[] values) {
        this.sensorType = sensorType;
        this.timestamp = timestamp;
        this.values = values;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public float[] getValues() {
        return values;
    }

    public void setValues(float[] values) {
        this.values = values;
    }

}
