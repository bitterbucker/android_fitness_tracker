package com.rawfit.me.events;



public class StepSensor extends SensorEvent{

    private float step;

    public StepSensor(String sensorType, long timestamp, float[] values) {
        super(sensorType, timestamp, values);
        this.step = values[0];
    }

    public float getStep() {
        return step;
    }

    public void setStep(float step) {
        this.step = step;
    }

}
