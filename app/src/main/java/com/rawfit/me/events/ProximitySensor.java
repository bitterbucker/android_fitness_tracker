package com.rawfit.me.events;



public class ProximitySensor extends SensorEvent {

    private float proximity;

    public ProximitySensor(String sensorType, long timestamp, float[] values) {
        super(sensorType, timestamp, values);
        this.proximity=values[0];
    }

    public float getProximity() {
        return proximity;
    }

    public void setProximity(float proximity) {
        this.proximity = proximity;
    }


}