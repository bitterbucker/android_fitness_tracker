package com.rawfit.me.events;



public class Config {

    private String configType;
    private int value;
    private double threshold;

    public Config(String configType, int value) {
        this.configType = configType;
        this.value = value;
    }

    public Config(String configType, int value, double threshold) {
        this.configType = configType;
        this.value = value;
        this.threshold = threshold;
    }

    public String getConfigType() {
        return configType;
    }

    public void setConfigType(String configType) {
        this.configType = configType;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }
}
