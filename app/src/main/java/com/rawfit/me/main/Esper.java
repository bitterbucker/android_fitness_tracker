package com.rawfit.me.main;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.Vibrator;
import android.util.Log;

import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.espertech.esper.client.deploy.DeploymentException;
import com.espertech.esper.client.deploy.DeploymentOptions;
import com.espertech.esper.client.deploy.EPDeploymentAdmin;
import com.espertech.esper.client.deploy.Module;
import com.espertech.esper.client.deploy.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.events.AccelerometerSensor;
import com.rawfit.me.events.Config;
import com.rawfit.me.events.SensorEvent;
import com.rawfit.me.events.StepSensor;


public class Esper {

    private static final String TAG = "Esper";

    private EPServiceProvider engine;
    private SQLiteHelper db;
    private Configuration configuration;

    private static Esper esperInstance;

    public static synchronized Esper getEsperInstance(Context context,AssetManager assetManager) {
        if (esperInstance == null) {
            esperInstance = new Esper(context, assetManager);
        } else {
            //Log.d(TAG, "Esper already instantiated");
        }
        return esperInstance;
    }

    public static synchronized  Esper getEsper() {
        return esperInstance;
    }

    public EPServiceProvider getEngine() {
        return engine;
    }


    boolean vibrate = false;

    private void vibrate(Context context){
        if(vibrate==true) {
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(500);
        }
    }


    private Esper(final Context context, AssetManager assetManager) {
        //esper config
        configuration = new Configuration();
        configuration.getEngineDefaults().getExecution().setPrioritized(true);


        this.engine = EPServiceProviderManager.getDefaultProvider(configuration);
        //this.engine.getEPAdministrator().getConfiguration().addEventType(SensorEvent.class);
        this.engine.getEPAdministrator().getConfiguration().addEventType(AccelerometerSensor.class);
        this.engine.getEPAdministrator().getConfiguration().addEventType(StepSensor.class);
        this.engine.getEPAdministrator().getConfiguration().addEventType(Config.class);

        //Log.d(TAG, "--------------------------------------esper started--------------------------------------------");

        //db
        db = SQLiteHelper.getSqLiteHelper(context);


        try {
            //path to file from assets folder
            String s = readFromAssets(context, "main_module.epl");
            //String s1 = StepCounterActivity.readFromAssets(context, "healthyPaceModule.epl");

            AssetManager am = assetManager;

            InputStream is = am.open("main_module.epl");
            //InputStream is1 = am.open("healthyPaceModule.epl");

            //deployment modules
            EPDeploymentAdmin deployAdmin = engine.getEPAdministrator().getDeploymentAdmin();
            Module main = deployAdmin.read(is, s);
            //Module healthyPaceModule = deployAdmin.read(is1, s1);

            deployAdmin.deploy(main, new DeploymentOptions());
            //deployAdmin.deploy(healthyPaceModule, new DeploymentOptions());

            //attach listeners to statements in main module file
            engine.getEPAdministrator().getStatement("getCurrentHourCount").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {

                    double step = Double.parseDouble(eventBeen[0].get("HC").toString());
                    long timestamp = Long.parseLong(eventBeen[0].get("CT").toString());

                    //updates the current hour when the listener is notified
                    db.updateCurrentHour((int) step, timestamp);

                   // Log.d(TAG, eventBeen[0].getUnderlying()+" currHourUpdate or insert");
                }
            });



            engine.getEPAdministrator().getStatement("true").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {
                    //Log.d(TAG, eventBeen[0].getUnderlying() + "");

                    sendNotification(002, "Healthy pace detected. Keep walking!", /*eventBeen[0].getUnderlying()+*/" ", context);
                    vibrate(context);

                }
            });


           engine.getEPAdministrator().getStatement("false").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {
                    //Log.d(TAG, eventBeen[0].getUnderlying() + "");
                }
            });






           engine.getEPAdministrator().getStatement("dailyStepGoal").addListener(new UpdateListener() {
               @Override
               public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {

                   //have to reset the counter in the statement otherwise it wont trigger the following day
                   //TODO: use configevent instead
                   engine.getEPAdministrator().getStatement("dailyStepGoalAchievement").stop();
                   engine.getEPAdministrator().getStatement("dailyStepGoalAchievement").start();

                   //engine.getEPRuntime().sendEvent(new Config("resetHourCount", 0));
                   //engine.getEPRuntime().sendEvent(new Config("getCurrentHourCount", 0));
                   //Log.d(TAG, eventBeen[0].getUnderlying() + "");
               }
           });


            engine.getEPAdministrator().getStatement("newDay").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {

                    double calorie = Double.parseDouble(eventBeen[0].get("CALS").toString());
                    double weight = Double.parseDouble(eventBeen[0].get("WEIGHT").toString());
                    long timestamp = Long.parseLong(eventBeen[0].get("CT").toString());

                    db.insertCalorie((int)calorie, timestamp);
                    db.insertWeight((int)weight, timestamp);

                    //Log.d(TAG, eventBeen[0].getUnderlying() + "");
                }
            });


           engine.getEPAdministrator().getStatement("dailyStepGoalAchievement").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {

                    //insert into achievement table
                    db.insertAchievement("DSG", Long.parseLong(eventBeen[0].get("timestamp").toString()), Integer.parseInt(eventBeen[0].get("DG").toString()));

                    sendNotification(004, "Daily Goal Reached. Awesome!", /*eventBeen[0].getUnderlying()+*/"Daily Goal: " + eventBeen[0].get("DG").toString(), context);

                    vibrate(context);

                    //Log.d(TAG, eventBeen[0].getUnderlying() + "");
                }
            });


            engine.getEPAdministrator().getStatement("move").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {

                    //Log.d(TAG, eventBeen[0].getUnderlying()+" Be more active!");
                    sendNotification(003, "Be more active!", eventBeen[0].get("stepCount")+" steps the past hour.", context);

                    vibrate(context);
                }
            });



            engine.getEPAdministrator().getStatement("onWinSelect").addListener(new UpdateListener() {
                @Override
                public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {


                    //TODO: return time taken from epl instead
                    long t1 = Long.parseLong(eventBeen[0].get("A").toString());
                    long t2 = Long.parseLong(eventBeen[0].get("B").toString());

                    long diff = t2 - t1;

                    long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);

                    int hpCount = Integer.parseInt(eventBeen[0].get("HPCount").toString());

                    //insert healthypace session into table
                    db.insertHealthyPaceSession(seconds, t2, hpCount);


                    sendNotification(001, "Healthy Pace Recorded", Converter.secondsToHrsMinSec((int)seconds) + " and " +  " " + eventBeen[0].get("HPCount") + " steps recorded", context);

                    vibrate(context);

                    //Log.d(TAG,seconds + " sec of HP " + " and " + hpCount + " healthy steps");

                }
            });


        engine.getEPAdministrator().getStatement("updateEveryHour").addListener(new UpdateListener() {
            @Override
            public void update(final EventBean[] eventBeen, EventBean[] eventBeen1) {



                int step = Integer.parseInt(eventBeen[0].get("HC").toString());
                long timestamp = Long.parseLong(eventBeen[0].get("CT").toString());//current timestamp will always be 1 hr after what it should be inserted as, i.e, when the listener is updated the current hourcount will say hour 0 but the count applies for the prior hour, 23.
                //insert into db where hour H the current H
                Calendar calendar=Calendar.getInstance();
                calendar.setTimeInMillis(timestamp);
                calendar.add(Calendar.HOUR,-1);//insert for the hour before the hour that is shifted to


                //update hour row in step table before reseting hourcount otherwise the count is not stored before reset and as a result stored as 0
                db.updateCurrentHour( step, calendar.getTimeInMillis());

                engine.getEPRuntime().sendEvent(new Config("resetHourCount", 0));//reset the hourly count

                //Log.d(TAG, eventBeen[0].getUnderlying() + ""+calendar.getTime());
            }
        });


    } catch (IOException e) {
        e.printStackTrace();
    } catch (ParseException e) {
        e.printStackTrace();
    } catch (DeploymentException e) {
        e.printStackTrace();
    }





    }



    public static int getHourOfDay(long timesamp){
        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(timesamp);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);

        return hour;
    }


    private void sendNotification(int id, String title, String contentText, Context context) {
        //Get an instance of NotificationManager
        android.support.v7.app.NotificationCompat.Builder mBuilder = (android.support.v7.app.NotificationCompat.Builder) new android.support.v7.app.NotificationCompat.Builder(context)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(title)
                .setContentText(contentText);

        // Gets an instance of the NotificationManager service//
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //When you issue multiple notifications about the same type of event, it’s best practice for your app to try to update an existing notification with this new information, rather than immediately creating a new notification. If you want to update this notification at a later date, you need to assign it an ID. You can then use this ID whenever you issue a subsequent notification. If the previous notification is still visible, the system will update this existing notification, rather than create a new one. In this example, the notification’s ID is 001//

        mNotificationManager.notify(id, mBuilder.build());
    }

    //read from assets file
    public static String readFromAssets(Context context, String filename) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)));
        StringBuilder sb = new StringBuilder();
        String mLine = br.readLine();
        while (mLine != null) {
            sb.append(mLine);
            mLine = br.readLine();
        }
        br.close();
        return sb.toString();
    }

}
