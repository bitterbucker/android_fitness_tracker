package com.rawfit.me.main;



import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.events.Config;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.view.ColumnChartView;
import com.rawfit.me.stepcounter.AggregationValue;
import com.rawfit.me.stepcounter.StepCounterActivity;
import com.squareup.leakcanary.LeakCanary;

import static com.rawfit.me.main.Dashboard.initLeakCanary;
import static com.rawfit.me.stepcounter.StepCounterActivity.calsPerStep;
import static com.rawfit.me.stepcounter.StepCounterActivity.user;



public class TabFragment1 extends Fragment {

    private static final String TAG = "TabFragment1";

    private static final int HOURS_IN_A_DAY = 24;

    //TextViews
    private TextView stepTv = null;
    private TextView calsBurnedTv = null;
    private TextView healthyPaceTv = null;
    private TextView healthyPaceDurationTv = null;
    private TextView distanceTv=null;

    //sqlite db
    private SQLiteHelper db;

    //counters
    private int stepCounter;
    private int activeDuration;
    private double healthyPaceStepCount;
    //private double currentHourlyCount;//TODO: remove

    //column chart
    private ColumnChartView chart;
    private List<SubcolumnValue> values;
    private ColumnChartData data;

    private boolean isPaused;

    //TODO: use subscriber object instead of update listener

    private Esper esper;

    private EPStatement getStepStmt;
    private UpdateListener stepListener;

    private EPStatement getHealthyPaceStmt;
    private UpdateListener healthyPaceListener;

    private EPStatement dateChangeSmt;
    private UpdateListener dateChangeListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Log.d("ServiceTest", "onCreate() fragment");

        initLeakCanary(getActivity().getApplication(), getContext());


        //db
        db = SQLiteHelper.getSqLiteHelper(getContext());
        esper = Esper.getEsper();

        //
        getStepStmt = esper.getEngine().getEPAdministrator().getStatement("step");
        stepListener = new UpdateListener() {
            @Override
            public void update(EventBean[] eventBeen, EventBean[] eventBeen1) {

                //Log.d(TAG, step+" from esper");
                stepCounter++;
                final int stepCount = (int) stepCounter;
                final int calsBurned = (int) calsPerStep(user, stepCounter);

                final TextView hptv=(TextView)getActivity().findViewById(R.id.stepTv);
                final TextView cbtv=(TextView)getActivity().findViewById(R.id.calsBurnedTv);
                final TextView dtv=(TextView)getActivity().findViewById(R.id.distanceTv);

                final String unit = user.getUnit();

                hptv.post(new Runnable() {
                    @Override
                    public void run() {
                        hptv.setText(stepCount+"/"+ user.getStepGoal());
                    }
                });
                cbtv.post(new Runnable() {
                    @Override
                    public void run() {
                        cbtv.setText(calsBurned+"");
                    }
                });
                dtv.post(new Runnable() {
                    @Override
                    public void run() {
                        if(unit.equals("metric")){
                            dtv.setText((user.getStrideLength()*stepCounter) / 1000 + " km");
                        }
                        if(unit.equals("imperial")) {
                            dtv.setText((user.getStrideLength() * stepCounter) / 1000 * (0.62137) + " mi");
                        }
                    }
                });

                //update current hour column
                int colCount=0;
                for (Column column : data.getColumns()) {
                    for (SubcolumnValue value : column.getValues()) {
                        //Log.d(TAG, "foreach " + value + " colCount " + colCount + " date " + new Date(System.currentTimeMillis()).getHours());
                        if (colCount == new Date().getHours()) {
                            value.setValue(value.getValue() + 1);
                        }
                    }
                    colCount++;
                }
                chart.setColumnChartData(data);

            }
        };


                //
                getHealthyPaceStmt = esper.getEngine().getEPAdministrator().getStatement("onWinSelect");
                healthyPaceListener  = new UpdateListener() {
                    @Override
                    public void update(EventBean[] eventBeen, EventBean[] eventBeen1) {
                        long t1 = Long.parseLong(eventBeen[0].get("A").toString());
                        long t2 = Long.parseLong(eventBeen[0].get("B").toString());

                        long diff;

                        if(t2>=t1) {
                            diff = t2 - t1;
                        }
                        else{
                            diff = t1 - t2;
                        }

                        long seconds = TimeUnit.MILLISECONDS.toSeconds(diff);
                        long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);

                        int hpCount = Integer.parseInt(eventBeen[0].get("HPCount").toString());


                        activeDuration+=seconds;
                        healthyPaceStepCount+=hpCount;

                        final TextView hptv=(TextView)getActivity().findViewById(R.id.healthyPaceTv);
                        final TextView hpdtv=(TextView)getActivity().findViewById(R.id.healthyPaceDurationTv);

                        hptv.post(new Runnable() {
                            @Override
                            public void run() {
                                hptv.setText(""+healthyPaceStepCount);
                            }
                        });

                        hpdtv.post(new Runnable() {
                            @Override
                            public void run() {
                                hpdtv.setText(""+ Converter.secondsToHrsMinSec(activeDuration));
                            }
                        });

                        //Log.d(TAG, hpCount + " " + seconds + " healthyPaceListener");

                    }
                };


                //TODO: use inset stream for this stmt
                dateChangeSmt = esper.getEngine().getEPAdministrator().getStatement("dailyStepGoal");
                dateChangeListener = new UpdateListener() {
                    @Override
                    public void update(EventBean[] eventBeen, EventBean[] eventBeen1) {

                        //reset counters
                        stepCounter = 0;
                        activeDuration = 0;
                        healthyPaceStepCount = 0;
                        //currentHourlyCount=0;//TODO: see if this fix the problem with hour 0 being carried over to the new date //TODO: remove

                        getActivity().runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //TODO: remove if unecssary
                                new DoInBg().execute("stepsPrHour");
                                new DoInBg().execute("totalSteps");
                                new DoInBg().execute("healthyPace");
                            }
                        });

                        //Log.d(TAG, user.toString() + " dateChangeListener from date changer esper");

                    }
                };

        //register listeners
        getStepStmt.addListener(stepListener);
        getHealthyPaceStmt.addListener(healthyPaceListener);
        dateChangeSmt.addListener(dateChangeListener);

        isPaused=false;

        getCurrentHour();

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //Log.d(TAG, "onViewCreate() fragment");

        //text views
        stepTv = (TextView) view.findViewById(R.id.stepTv);
        calsBurnedTv = (TextView) view.findViewById(R.id.calsBurnedTv);
        healthyPaceTv = (TextView) view.findViewById(R.id.healthyPaceTv);
        healthyPaceDurationTv = (TextView) view.findViewById(R.id.healthyPaceDurationTv);
        distanceTv = (TextView) view.findViewById(R.id.distanceTv);

        chart = (ColumnChartView) view.findViewById(R.id.step24hrChart);
        initBlankChart();

        new DoInBg().execute("stepsPrHour");
        new DoInBg().execute("totalSteps");
        new DoInBg().execute("healthyPace");

        chart.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
            @Override
            public void onValueSelected(int i, int i1, SubcolumnValue subcolumnValue) {
                Toast.makeText(getContext(),subcolumnValue.getValue()+"", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onValueDeselected() {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Log.d(TAG, "onCreateView() fragment1");
        return inflater.inflate(R.layout.tab_fragment_1, container, false);
    }


    private void addListeners(){

    }

    @Override
    public void onResume(){
        super.onResume();

        if(isPaused==true){

            //register listener
            getStepStmt.addListener(stepListener);
            getHealthyPaceStmt.addListener(healthyPaceListener);
            dateChangeSmt.addListener(dateChangeListener);

            getCurrentHour();

            new DoInBg().execute("stepsPrHour");
            new DoInBg().execute("totalSteps");
            new DoInBg().execute("healthyPace");

            //Log.d(TAG, "fragment onResume() - re-register listeners");
        }
        else{

            //Log.d(TAG, "fragment onResume()");
        }


    }



    @Override
    public void onPause(){
        super.onPause();

        getCurrentHour();

        isPaused=true;

        //unregister all listeners otherwise you will receive multiple updates when statements fires
        getStepStmt.removeListener(stepListener);
        getHealthyPaceStmt.removeListener(healthyPaceListener);
        dateChangeSmt.removeListener(dateChangeListener);

        //Log.d(TAG, "fragment1 onPause()");
    }




    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            //Log.d(TAG, "visible fragment 1");
        } else {
            //Log.d(TAG, "invisible fragment 1");
        }
    }


    //send an event to notify listener in esper to insert or update the latest hourly stepcount
    public static void getCurrentHour(){
        try {
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new Config("getCurrentHourCount", 0));
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    private void initBlankChart() {
        //hour count list
        List<AggregationValue> aggregationValues = new ArrayList<>();
        for (int i = 0; i < 24; i++) {
            aggregationValues.add(i, new AggregationValue());
        }

        //column chart
        //chart = (ColumnChartView) getActivity().findViewById(R.id.step24hrChart);
        List<AxisValue> axisValues = new ArrayList<AxisValue>();
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values = null;

        for (int i = 0; i < 24; ++i) {
            values = new ArrayList<SubcolumnValue>();
            //for (int j = 0; j < aggregationValues.size(); ++j) {
            if (aggregationValues.get(i).getDate() == i) {
                values.add(new SubcolumnValue(aggregationValues.get(i).getValue()).setColor(Dashboard.colColor));
            }
            //}
            axisValues.add(new AxisValue(i).setLabel(i + ""));
            columns.add(new Column(values).setHasLabels(false));
        }

        data = new ColumnChartData(columns);

        data.setAxisXBottom(new Axis(axisValues).setHasLines(false).setName("Hour").setTextColor(Dashboard.colColor));

        chart.setZoomType(ZoomType.HORIZONTAL);


        chart.setColumnChartData(data);
    }

    private class DoInBg extends AsyncTask<String, Integer, Cursor> {

        String query;

        @Override
        protected void onPreExecute() {
            //reset counters
            stepCounter=0;
            activeDuration=0;
            healthyPaceStepCount=0;
            //currentHourlyCount=0;
            //Log.d(TAG," Task Starting...");
        }


        @Override
        protected Cursor doInBackground(String... params) {

            db = SQLiteHelper.getSqLiteHelper(getContext());

            if(params[0].equals("stepsPrHour")){
                query="stepsPrHour";
                return db.getTotalStepsPrHour();
            }
            else if(params[0].equals("totalSteps")){
                query="totalSteps";
                return db.getTotalSteps();
            }
            else if(params[0].equals("healthyPace")){
                query="healthyPace";
                return db.getHealthyPace();
            }

            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... params) {

        }

        @Override
        protected void onPostExecute(Cursor result) {

            if(query.equals("healthyPace")){
                int stepCount=0;
                long duration=0;
                result.moveToFirst();
                while (result.isAfterLast() == false) {
                    //Log.d(TAG,result.getString(0)+" "+result.getString(1));
                    if(result.getString(0)!=null) {
                        duration = (int) Long.parseLong(result.getString(0));
                        stepCount = Integer.parseInt(result.getString(1));

                        activeDuration = (int) duration;
                        healthyPaceStepCount = stepCount;

                    }

                    result.moveToNext();
                }
                result.close();

                healthyPaceTv.setText(""+stepCount+" steps");

                healthyPaceDurationTv.setText(""+ Converter.secondsToHrsMinSec(activeDuration));


            }
            if(query.equals("totalSteps")){
                double stepCount=0;
                result.moveToFirst();
                while (result.isAfterLast() == false) {
                    if(result.getString(0)!=null){
                            stepCount = Double.parseDouble(result.getString(0));
                        //Log.d(TAG,stepCount + " total steps");
                            stepCounter += (int) stepCount;

                        //Log.d(TAG,"current step set to " + stepCounter);
                        }
                    result.moveToNext();
                }
                result.close();

                //TODO: check if needed
                user.setCurrentStep(stepCounter);

                stepTv.setText(stepCounter+"/"+user.getStepGoal());
                calsBurnedTv.setText(calsPerStep(user,stepCounter)+"");

                if(user.getUnit().equals("metric")){
                    distanceTv.setText((user.getStrideLength()*stepCounter)/1000 + " km");
                }
                if(user.getUnit().equals("imperial")){
                    distanceTv.setText((user.getStrideLength()*stepCounter)/1000*(0.62137) + " mi");
                }


            }
            if(query.equals("stepsPrHour")) {

                //hour count list
                //List<AggregationValue> aggregationValues = new ArrayList<>();
                AggregationValue[] aggregationValues = new AggregationValue[24];

                for (int i = 0; i < HOURS_IN_A_DAY; i++) {
                    //aggregationValues.add(i, new AggregationValue());
                    aggregationValues[i] = new AggregationValue();
                    aggregationValues[i].setDate(i);
                    aggregationValues[i].setValue(0);
                }


                result.moveToFirst();
                while (result.isAfterLast() == false) {
                    if (result.getString(0) != null) {
                        int hour = Integer.parseInt(result.getString(1));
                        int hourlyCount = Integer.parseInt(result.getString(0));
                        //aggregationValues.get(hour).setDate(hour);
                        //aggregationValues.get(hour).setValue(hourlyCount);
                        //aggregationValues[hour].setDate(hour);
                        //aggregationValues[hour].setValue(hourlyCount);

                        for (int i = 0; i < HOURS_IN_A_DAY; i++) {

                            if (aggregationValues[i].getDate() == hour) {
                                aggregationValues[i].setValue(hourlyCount);
                            }

                        }

                        //Log.d(TAG, " hour from db " + hour + " " + hourlyCount);
                    }
                    result.moveToNext();
                }
                result.close();

                //column chart
                //chart = (ColumnChartView) getActivity().findViewById(R.id.step24hrChart);
                List<AxisValue> axisValues = new ArrayList<AxisValue>();
                List<Column> columns = new ArrayList<Column>();
                //List<SubcolumnValue> values = null;


                for (int i = 0; i < HOURS_IN_A_DAY; ++i) {
                    values = new ArrayList<SubcolumnValue>();

                    //adds the current hour count to the column value for the current hour
                    if (aggregationValues[i].getDate() == new Date().getHours()) {
                        values.add(new SubcolumnValue((float) (aggregationValues[i].getValue()/*+currentHourlyCount*/)).setColor(Dashboard.colColor));
                    } else if (aggregationValues[i].getDate() == i) {

                        values.add(new SubcolumnValue((float) (aggregationValues[i].getValue())).setColor(Dashboard.colColor));
                        //Log.d(TAG, " getDateCheck1 " + i);
                    } else {
                        values.add(new SubcolumnValue(0).setColor(Dashboard.colColor));
                        //Log.d(TAG, " getDateCheck3 " + i);
                    }

                    axisValues.add(new AxisValue(i).setLabel(i + ""));
                    columns.add(new Column(values).setHasLabels(false));
                }

                data = new ColumnChartData(columns);
                data.setAxisXBottom(new Axis(axisValues).setHasLines(false).setName("Hour").setTextColor(Dashboard.colColor));
                chart.setZoomType(ZoomType.HORIZONTAL);
                chart.setColumnChartData(data);


            }

        }
    }
}