package com.rawfit.me.main;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.oss.licenses.OssLicensesMenuActivity;

import com.rawfit.me.SQL.SQLiteHelper;
import com.squareup.leakcanary.LeakCanary;

import static com.rawfit.me.main.Dashboard.animate;
import static com.rawfit.me.main.Dashboard.initLeakCanary;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";

    //db
    private SQLiteHelper db;
    //tv
    private TextView totalStepCountOfAllTime;
    private TextView totalWalkingDistanceOfAllTime;
    private TextView licenseLabel;
    //user
    private User user;

    //et
    private EditText weightEt;
    private EditText heightEt;
    private EditText calGoalInput;
    private EditText stepGoalInput;
    private EditText strideEt;
    private EditText ageEt;

    //count changes
    int changeCounter=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        initLeakCanary(getApplication(), this);




        setContentView(R.layout.activity_profile);


        getSupportActionBar().setTitle("Settings");


        //animation
        animate(getWindow());


        //textview
        totalStepCountOfAllTime = (TextView) findViewById(R.id.totalStepCountOfAllTime);
        totalWalkingDistanceOfAllTime = (TextView) findViewById(R.id.totalDistanceOfAllTime);
        licenseLabel = (TextView) findViewById(R.id.licenseLabel);


        licenseLabel.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                Intent intent = new Intent(getApplicationContext(), OssLicensesMenuActivity.class);
                String title = "Open Source Third-Party Libraries";
                intent.putExtra("title", title);
                startActivity(intent);


                return false;
            }
        });


        //edit text
        weightEt = (EditText) findViewById(R.id.weightInput);
        weightEt.setTextColor(Color.WHITE);
        weightEt.setHintTextColor(Color.WHITE);
        heightEt = (EditText) findViewById(R.id.heightInput);
        heightEt.setTextColor(Color.WHITE);
        heightEt.setHintTextColor(Color.WHITE);

        strideEt = (EditText) findViewById(R.id.strideInput);
        strideEt.setTextColor(Color.WHITE);
        strideEt.setHintTextColor(Color.WHITE);

        calGoalInput = (EditText) findViewById(R.id.calGoalInput);
        calGoalInput.setTextColor(Color.WHITE);
        calGoalInput.setHintTextColor(Color.WHITE);

        stepGoalInput = (EditText) findViewById(R.id.stepGoalInput);
        stepGoalInput.setTextColor(Color.WHITE);
        stepGoalInput.setHintTextColor(Color.WHITE);

        ageEt = (EditText) findViewById(R.id.ageEt);
        ageEt.setTextColor(Color.WHITE);
        ageEt.setHintTextColor(Color.WHITE);

        //user
        user = (User) getIntent().getSerializableExtra("userInfo");
        //Log.d(TAG, user.toString());

        //db
        db = SQLiteHelper.getSqLiteHelper(getBaseContext());
        final double totalStepsD = db.getTotalStepCountOfAllTime();
        final int totalSteps = (int)totalStepsD;

        // initiate a Switch
        final Switch unitSwitch = (Switch) findViewById(R.id.kgToLbsSwitch);
        unitSwitch.setTextColor((Color.WHITE));
        if(user.getUnit().equals("metric")){

            weightEt.setHint(user.getWeight()+"kg");
            heightEt.setHint(user.getHeight()+"cm");
            strideEt.setHint(user.getStrideLength() * 100 +" cm");

            unitSwitch.setChecked(false);
            unitSwitch.setText("Metric");
            //Log.d(TAG, "false metric " + user.toString());
        }
        if(user.getUnit().equals("imperial")) {

            weightEt.setHint(user.getWeight() + "lbs");
            heightEt.setHint(Converter.inchToFeetIn(user.getHeight())[2] + "");
            strideEt.setHint(Converter.cmToIn(user.getStrideLength() * 100) + " in");

            unitSwitch.setChecked(true);
            unitSwitch.setText("Imperial");

            //Log.d(TAG, "false metric " + user.toString());
        }

        //start at boot switch
        final Switch startAtBootSwitch = (Switch) findViewById(R.id.starAtBootSwitch);
        startAtBootSwitch.setTextColor((Color.WHITE));

        if(user.getStartAtBoot()==0){
            startAtBootSwitch.setChecked(false);
            startAtBootSwitch.setText("Off");
        }else{
            startAtBootSwitch.setChecked(true);
            startAtBootSwitch.setText("On");
        }

        startAtBootSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b==false){
                    user.setStartAtBoot(0);
                    startAtBootSwitch.setText("Off");
                    changeCounter++;
                }
                if(b==true){
                    user.setStartAtBoot(1);
                    startAtBootSwitch.setText("On");
                    changeCounter++;
                }

            }


        });





        //step sensor switch
        final Switch stepSensorTypeSwitch = (Switch) findViewById(R.id.stepSensorTypeSwitch);
        stepSensorTypeSwitch.setTextColor((Color.WHITE));

        if(user.getStepSensorType()==0){
            stepSensorTypeSwitch.setChecked(false);
            stepSensorTypeSwitch.setText("Default");
        }else{
            stepSensorTypeSwitch.setChecked(true);
            stepSensorTypeSwitch.setText("Experimental");
        }

        SensorManager mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        Sensor mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        stepSensorTypeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b==false){
                    user.setStepSensorType(0);
                    stepSensorTypeSwitch.setText("Default");
                    changeCounter++;

                    //TODO: restart service
                    /*stopService(new Intent(getApplicationContext(), BackgroundService.class));
                    startService(new Intent(getApplicationContext(), BackgroundService.class));*/

                    Intent intent = new Intent("stepSensorChanged");
                    intent.putExtra("value",0);
                    sendBroadcast(intent);

                    //Log.d(TAG, "service restarted");

                }
                if(b==true){
                    user.setStepSensorType(1);
                    stepSensorTypeSwitch.setText("Experimental");
                    changeCounter++;

                    //TODO: restart service
                    /*stopService(new Intent(getApplicationContext(), BackgroundService.class));
                    startService(new Intent(getApplicationContext(), BackgroundService.class));*/

                    Intent intent = new Intent("stepSensorChanged");
                    intent.putExtra("value",1);
                    sendBroadcast(intent);

                    //Log.d(TAG, "service restarted");
                }

            }


        });




        //spinner for gender
        Spinner spinner = (Spinner) findViewById(R.id.genderSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.gender_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);





        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int ig, long l) {
                if(adapterView.getSelectedItem().equals("Unknown")) {
                        user.setGender(User.Gender.UNKNOWN);
                        changeCounter++;
                }
                if(adapterView.getSelectedItem().equals("Male")){
                        user.setGender(User.Gender.MALE);
                        changeCounter++;
                }
                if(adapterView.getSelectedItem().equals("Female")) {
                        user.setGender(User.Gender.FEMALE);
                        changeCounter++;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });







        // check current state of a Switch (true or false).
        Boolean switchState = unitSwitch.isChecked();

        unitSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b==false){
                    user.setUnit("metric");
                    user.setHeight(Converter.inToCm(user.getHeight()));
                    user.setWeight(Converter.lbsToKg(user.getWeight()));
                    weightEt.setHint(user.getWeight()+"kg");
                    heightEt.setHint(user.getHeight()+"cm");
                    unitSwitch.setText("Metric");

                    strideEt.setHint(user.getStrideLength() +" m");

                    totalWalkingDistanceOfAllTime.setText("Total distance walked: " + (user.getStrideLength()*totalSteps)/1000 + " km");

                    changeCounter++;

                    //Log.d(TAG, "false metric " + user.toString());
                }
                if(b==true){
                    user.setUnit("imperial");
                    user.setHeight(Converter.cmToIn(user.getHeight()));
                    user.setWeight(Converter.kgToLbs(user.getWeight()));
                    weightEt.setHint(user.getWeight()+"lbs");
                    //heightEt.setHint(user.getHeight()+"inch");
                    heightEt.setHint(Converter.inchToFeetIn(user.getHeight())[2]+"");
                    unitSwitch.setText("Imperial");

                    strideEt.setHint(Converter.cmToIn(user.getStrideLength() )+" ft");

                    totalWalkingDistanceOfAllTime.setText("Total distance walked: " + (user.getStrideLength() * totalSteps) / 1000 * (0.62137) + " mi");

                    changeCounter++;

                    //Log.d(TAG, "true imperial " + user.toString());
                }

            }


        });


        //weightEt.setHint(user.getWeight()+"kg");
        //heightEt.setHint(user.getHeight()+"cm");
        calGoalInput.setHint((user.getCalorieBurnedPerDayGoal()+" calories"));
        stepGoalInput.setHint(user.getStepGoal()+ " steps");
        ageEt.setHint(user.getAge()+" yrs");

        //totals information
        totalStepCountOfAllTime.setText("Total steps walked: " + totalSteps);

        String unit = user.getUnit();
        if(unit.equals("metric")){
            totalWalkingDistanceOfAllTime.setText("Total distance walked: " + (user.getStrideLength()*totalSteps)/1000 + " km");
        }
        if(unit.equals("imperial")) {
            totalWalkingDistanceOfAllTime.setText("Total distance walked: " + (user.getStrideLength() * totalSteps) / 1000 * (0.62137) + " mi");
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        //Log.d(TAG, "onStop()");
    }

    @Override
    public void onPause() {
        super.onPause();
        //Log.d(TAG, "onPause");
    }



    @Override
    public void onBackPressed() {

        int weight=0;
        int height=0;
        int stepGoal=0;
        int calGoal=0;
        double stride=0;
        int age=0;

        if(!weightEt.getText().toString().equals("")){
            weight = Integer.parseInt(weightEt.getText().toString());
            user.setWeight(weight);
            changeCounter++;
        }
        if(!heightEt.getText().toString().equals("")){
            height = Integer.parseInt(heightEt.getText().toString());
            user.setHeight(height);
            changeCounter++;
        }
        if(!strideEt.getText().toString().equals("")){
            stride = Integer.parseInt(strideEt.getText().toString());
            user.setStride(stride/100);//user types in cm but stored as m
            changeCounter++;
        }
        if(!stepGoalInput.getText().toString().equals("")) {
            stepGoal = Integer.parseInt(stepGoalInput.getText().toString());
            user.setStepGoal(stepGoal);

            //TODO: user config event instead
            Esper.getEsper().getEngine().getEPAdministrator().getStatement("dailyStepGoalInitial").stop();
            Esper.getEsper().getEngine().getEPAdministrator().getStatement("dailyStepGoalInitial").start();

            Esper.getEsper().getEngine().getEPAdministrator().getStatement("dailyStepGoalAchievement").stop();
            Esper.getEsper().getEngine().getEPAdministrator().getStatement("dailyStepGoalAchievement").start();


            changeCounter++;
        }
        if(!calGoalInput.getText().toString().equals("")) {
            calGoal = Integer.parseInt(calGoalInput.getText().toString());
            user.setCalorieBurnedPerDayGoal(calGoal);
            changeCounter++;
        }
        if(!ageEt.getText().toString().equals("")) {
            age = Integer.parseInt(ageEt.getText().toString());
            user.setAge(age);
            changeCounter++;
        }

        //TODO: check later
        /*if(!thresholdEt.getText().toString().equals("")){
            double threshold = Double.parseDouble(thresholdEt.getText().toString());
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new Config("changeThreshold", 0, threshold));
        }
        if(!repetitionEt.getText().toString().equals("")){
            int value = Integer.parseInt(repetitionEt.getText().toString());
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new Config("changeStepRepititions", value, value));

        }*/
        //only update if changes were made
        if(changeCounter!=0) {

            db.updateUserData(user);

            Toast.makeText(getApplicationContext(), "Profile Settings Saved", Toast.LENGTH_SHORT).show();

            //TODO: testing passing user object as intent back to main activity
            Intent intent = new Intent();
            intent.putExtra("userInfoUpdate", user);
            setResult(Dashboard.REQUESTCODE_USER, intent);
            finish();
        }

        super.onBackPressed();
        //Log.d(TAG, "onBackPressed()");
    }

}
