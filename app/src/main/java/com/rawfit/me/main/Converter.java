package com.rawfit.me.main;



public class Converter {


    public static double cmToIn(double cm){

        return cm/2.54;
    }
    public static double inToCm(double in){

        return in*2.54;
    }

    public static double kgToLbs(double kg){

        return kg*2.2;
    }
    public static double lbsToKg(double lbs) {

        return lbs / 2.2;
    }


    public static String secondsToHrsMinSec(int tot_seconds){
        int hours = tot_seconds / 3600;
        int minutes = (tot_seconds % 3600) / 60;
        int seconds = tot_seconds % 60;

        String timeString = String.format("%02d hr %02d min %02d sec ", hours, minutes, seconds);

        System.out.println(timeString);

        return timeString;
    }




    public static String[] inchToFeetIn(double inches){

        int feet =(int) inches / 12;
        int leftover = (int) inches % 12;
        System.out.println(feet + " ft " + leftover + '"');

        String[] ftIn = new String[3];
        ftIn[0]=feet+"";
        ftIn[1]=leftover+"";
        ftIn[2]=feet+" ft"+ leftover +" in";
        return ftIn;
    }


    public static double feetInchToInch(String feet, String inch) {

        //String[] split = ftIn.split(";");
        int ft = Integer.parseInt(feet);
        int in = Integer.parseInt(inch);

        double inches = ft * 12;

        return inches + in;
    }

}
