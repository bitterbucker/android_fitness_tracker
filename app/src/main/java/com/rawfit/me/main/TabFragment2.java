package com.rawfit.me.main;


import android.content.Intent;
import android.database.Cursor;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rawfit.me.SQL.SQLiteHelper;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.listener.ViewportChangeListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;
import lecho.lib.hellocharts.view.PreviewLineChartView;
import com.rawfit.me.stepcounter.AggregateInformation;
import com.rawfit.me.stepcounter.AggregationValue;
import com.rawfit.me.stepcounter.ScrollingActivity;
import com.rawfit.me.stepcounter.StepCounterActivity;

import static com.rawfit.me.main.Dashboard.initLeakCanary;
import static com.rawfit.me.stepcounter.StepCounterActivity.user;



@RequiresApi(api = Build.VERSION_CODES.N)
public class TabFragment2 extends Fragment {

    private static final String TAG = "TabFragment2";

    static Calendar now = Calendar.getInstance();
    public static int YEAR = now.get(Calendar.YEAR);

    //time units
    static final int WEEKS_IN_A_YEAR = 52+1;//because of strftime()%W week of year: 00-53
    static final int MONTHs_IN_A_YEAR = 12;// %m month: 01-12
    static final int DAYS_IN_A_YEAR = 365+1;//because of strftime()%j day of year: 001-366

    //MONTHS
    public static final String[] MONTHS = new String[]{"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    //line chart
    private LineChartView lineChartView;
    private LineChartData lineChartData;

    //preview chart
    private PreviewLineChartView previewLineChartView;
    private LineChartData previewLineChartData;



    //spinner query type
    private String query;
    //chart X axis label
    private String label;
    //current chart view, day, week or month
    String viewType;



    //db
    private SQLiteHelper db;





    //tv
    //public static TextView datePickerLabel;//TODO: remove
    private TextView chartDate;


    private boolean isPaused;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        initLeakCanary(getActivity().getApplication(), getContext());


        db = SQLiteHelper.getSqLiteHelper(getContext());
    }

    public void init(View view){

        //line chart for average steps per week
        lineChartView = (LineChartView) view.findViewById(R.id.step24hrChart);
        previewLineChartView = (PreviewLineChartView) view.findViewById(R.id.previewChart);

        //spinner time unit
        Spinner spinner = view.findViewById(R.id.dmw_spinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.unit_of_time_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int ig, long l) {
                if(adapterView.getSelectedItem().equals("Daily")) {
                    viewType="Daily";
                    new DoInBg().execute("daily");

                }
                if(adapterView.getSelectedItem().equals("Weekly")){
                    viewType="Weekly";
                    new DoInBg().execute("weekly");

                }
                if(adapterView.getSelectedItem().equals("Monthly")){
                    viewType="Monthly";
                    new DoInBg().execute("monthly");

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //spinner 2 YEAR
        Spinner spinner2 = view.findViewById(R.id.dmw_spinner2);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getContext(), R.array.year_array, R.layout.spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter2.setDropDownViewResource(R.layout.spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner2.setAdapter(adapter2);

        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int ig, long l) {
                if(adapterView.getSelectedItem().equals("2017")) {

                    YEAR =2017;
                    //chartDate.setText("2017");
                    if(viewType.equals("Daily")) {
                        //viewType="Daily";
                        new DoInBg().execute("daily");


                    }
                    if(viewType.equals("Weekly")){
                        //viewType="Weekly";
                        new DoInBg().execute("weekly");


                    }
                    if(viewType.equals("Monthly")){
                        //viewType="Monthly";
                        new DoInBg().execute("monthly");


                    }

                }
                if(adapterView.getSelectedItem().equals("2018")){

                    YEAR = 2018;
                    //chartDate.setText("2018");
                    if(viewType.equals("Daily")) {
                        //viewType="Daily";
                        new DoInBg().execute("daily");


                    }
                    if(viewType.equals("Weekly")){
                        //viewType="Weekly";
                        new DoInBg().execute("weekly");


                    }
                    if(viewType.equals("Monthly")){
                        //viewType="Monthly";
                        new DoInBg().execute("monthly");


                    }
                }
                if(adapterView.getSelectedItem().equals("2019")){

                    YEAR = 2019;
                    //chartDate.setText("2019");
                    if(viewType.equals("Daily")) {
                        //viewType="Daily";
                        new DoInBg().execute("daily");


                    }
                    if(viewType.equals("Weekly")){
                        //viewType="Weekly";
                        new DoInBg().execute("weekly");


                    }
                    if(viewType.equals("Monthly")){
                        //viewType="Monthly";
                        new DoInBg().execute("monthly");


                    }
                }
                if(adapterView.getSelectedItem().equals("2020")){

                    YEAR = 2020;
                    chartDate.setText("2020");
                    if(viewType.equals("Daily")) {
                        //viewType="Daily";
                        new DoInBg().execute("daily");

                    }
                    if(viewType.equals("Weekly")){
                        //viewType="Weekly";
                        new DoInBg().execute("weekly");

                    }
                    if(viewType.equals("Monthly")){
                        //viewType="Monthly";
                        new DoInBg().execute("monthly");

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isPaused=false;

        init(view);

    }


    public static String getDatesBetweenWeekNumber(int weekNumber){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.WEEK_OF_YEAR, weekNumber);
        Date yourDate = cal.getTime();

        cal.setTime(yourDate);//Set specific Date of which start and end you want

        Date start,end;

        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        start = cal.getTime();//Date of Monday of current week

        cal.add(Calendar.DATE, 6);//Add 6 days to get Sunday of next week
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        end = cal.getTime();//Date of Sunday of current week
        System.out.println(start +" - "+ end);


        return new SimpleDateFormat("dd/MM").format(start)+"-"+new SimpleDateFormat("dd/MM").format(end);
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String getDateByDayNumber(int dayNumber, int year){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, Calendar.JANUARY);
        c.set(Calendar.DATE, 1);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        c.add(Calendar.DATE, dayNumber);
        Date date = c.getTime();

        //return new SimpleDateFormat("yyyy-MM-dd").format(date);
        return new SimpleDateFormat("dd/MM").format(date);
    }

    // returns the last index based of days in a YEAR for the viewport preview
    private int findLastIndex(AggregationValue[] list){

        int t=0;
        for (int i = 0; i < list.length; i++) {
            if (list[i] != null) {
                t = i;
            }
        }
        //Log.d(TAG, ""+ t);
        return t;
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_fragment_2, container, false);
    }


    //asynctask
    private class DoInBg extends AsyncTask<String, Integer, Cursor> {

        @Override
        protected void onPreExecute() {
            //Log.d(TAG, " Task Starting...");
        }

        @Override
        protected Cursor doInBackground(String... params) {

            db = SQLiteHelper.getSqLiteHelper(getContext());

            if (params[0].equals("daily")) {
                query = "daily";
                return db.getTotalStepsPerDay(YEAR);
            }
            if (params[0].equals("weekly")) {
                query = "weekly";
                return db.getAverageStepsPerWeek(YEAR);
            }
            if (params[0].equals("monthly")) {
                query = "monthly";
                return db.getAverageStepsPerMonth(YEAR);
            }
            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... params) {

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected void onPostExecute(Cursor result) {

            if(query.equals("daily")){
                generateLineChart(result,DAYS_IN_A_YEAR);
            }
            if(query.equals("weekly")) {
                generateLineChart(result,WEEKS_IN_A_YEAR);
            }
            if(query.equals("monthly")){
                generateLineChart(result,MONTHs_IN_A_YEAR);
            }

        }
    }

    //total steps for either month or week
    double totalStepsForMandW=0;

    private void generateLineChart(Cursor result, int arraySize){

        previewLineChartView.setVisibility(View.VISIBLE);

        //clear data
        lineChartView.getLineChartData().getLines().clear();

        AggregationValue[] aggregationValues = new AggregationValue[arraySize];//366//52//12


        /*for (int i = 0; i < aggregationValues.length; ++i) {
            aggregationValues[i]=new AggregationValue(i, 0);
        }*/



        List<AxisValue> axisValues = new ArrayList<>();
        List<AxisValue> axisValuesTop = new ArrayList<>();
        //List<AxisValue> previewAxisValues = new ArrayList<>();
        final List<Line> lines = new ArrayList<>();
        final List<Line> previewLines = new ArrayList<>();
        List<PointValue> values = new ArrayList<>();


        final double[] weeklyTotals = new double[53];
        final double[] monthlyTotals = new double[12];
        int counter = 0;
        result.moveToFirst();
        while (result.isAfterLast() == false) {
            //Log.d(TAG, result.getString(0) + " " + result.getString(1) + " day");
            int dayNumber=0;//TODO: change name
            double value=0;
            if(query.equals("monthly")){
                dayNumber = Integer.parseInt(result.getString(1))-1;
                value = Double.parseDouble(result.getString(0));
                totalStepsForMandW = Double.parseDouble(result.getString(2));

                monthlyTotals[counter]=totalStepsForMandW;
                Log.d(TAG, totalStepsForMandW + " month " + counter);
            }
            if(query.equals("weekly")) {
                dayNumber = Integer.parseInt(result.getString(1));
                value = Double.parseDouble(result.getString(0));
                totalStepsForMandW = Double.parseDouble(result.getString(4));
                weeklyTotals[counter]=totalStepsForMandW;
                Log.d(TAG, totalStepsForMandW + " weekly " + counter);
            }
            if(query.equals("daily")){
                dayNumber = Integer.parseInt(result.getString(1));
                value = Double.parseDouble(result.getString(0));
            }


            //if(value>0) {
                aggregationValues[dayNumber] = new AggregationValue(dayNumber, (int) value);
            //}
            result.moveToNext();

            counter++;
        }
        result.close();


        for (int i = 0; i < aggregationValues.length; ++i) {
            values.add(new PointValue(i, 0));

            if (aggregationValues[i] != null) {
                if (aggregationValues[i].getDate() == i) {
                    //values.add(new PointValue(i, aggregationValues[i].getValue())  ;
                    values.get(i).set(i, aggregationValues[i].getValue());
                    axisValuesTop.add(new AxisValue(i).setLabel(aggregationValues[i].getValue() + ""));
                }
            }

            if(query.equals("daily")) {
                label="Days";
                axisValues.add(new AxisValue(i).setLabel(getDateByDayNumber(i - 1, YEAR)));

            }
            if(query.equals("weekly")) {
                label="Weeks";
                axisValues.add(new AxisValue(i).setLabel("W "+(i)));
            }
            if(query.equals("monthly")) {
                label="Months";
                axisValues.add(new AxisValue(i).setLabel(MONTHS[i]));
            }

        }

        //line chart data
        Line line = new Line(values);
        line.setColor(Dashboard.colColor);
        line.setShape(ValueShape.CIRCLE);
        line.setCubic(true);
        line.setFilled(true);
        line.setHasLabels(false);
        line.setHasLabelsOnlyForSelected(false);
        line.setHasLines(true);
        line.setHasPoints(true);
        line.setStrokeWidth(1);
        lines.add(line);

        //preview line chart data
        previewLines.add(new Line(values).setHasLabelsOnlyForSelected(false)
                        .setFilled(true)
                        .setColor(Dashboard.colColor)
                        .setHasPoints(false).setStrokeWidth(1)
        );


        //liine chart
        lineChartData = new LineChartData(lines);
        lineChartData.setAxisXBottom(new Axis(axisValues).setHasSeparationLine(true).setMaxLabelChars(5).setHasLines(false).setName(label).setHasTiltedLabels(false).setTextColor(Dashboard.colColor));
        if(query.equals("daily")){
            lineChartData.setAxisXTop(new Axis(axisValuesTop).setHasLines(true).setMaxLabelChars(5).setInside(false).setName("Daily Total Steps").setHasTiltedLabels(false).setTextColor(Dashboard.colColor));

        }
        if(query.equals("weekly")){
            lineChartData.setAxisXTop(new Axis(axisValuesTop).setHasLines(true).setMaxLabelChars(5).setInside(false).setName("Average Daily Steps Per Week").setHasTiltedLabels(false).setTextColor(Dashboard.colColor));
        }
        if(query.equals("monthly")){
            lineChartData.setAxisXTop(new Axis(axisValuesTop).setHasLines(true).setMaxLabelChars(5).setInside(false).setName("Average Daily Steps Per Month").setHasTiltedLabels(false).setTextColor(Dashboard.colColor));
        }
        lineChartView.setLineChartData(lineChartData);


        //preview chart
        previewLineChartData = new LineChartData(previewLines);
        previewLineChartView.setLineChartData(previewLineChartData);
        lineChartView.setBackgroundColor(getResources().getColor(R.color.transparent2));
        previewLineChartView.setViewportChangeListener(new ViewportChangeListener() {
            @Override
            public void onViewportChanged(Viewport viewport) {

                lineChartView.setCurrentViewport(viewport);
            }
        });




        //TODO: fix viewport for weeks and months
        if(query.equals("daily")) {
            //set initial max viewport and current viewport- remember to set viewports after data.
            Viewport tempViewport = new Viewport(lineChartView.getMaximumViewport());
            float dx = findLastIndex(aggregationValues) - 17 / 1;
            tempViewport.inset(dx, 0);
            previewLineChartView.setCurrentViewport(tempViewport);

        }
        if(query.equals("weekly")) {
            Viewport tempViewport = new Viewport(lineChartView.getMaximumViewport());
            float dx = (findLastIndex(aggregationValues) - 17 ) / 2;
            tempViewport.inset(dx, 0);
            previewLineChartView.setCurrentViewport(tempViewport);


        }
        if(query.equals("monthly")) {



        }




        if(query.equals("daily")){
            lineChartView.setOnValueTouchListener(new LineChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int i, int i1, PointValue pointValue) {
                    Toast.makeText(getContext(),pointValue.getY()+"", Toast.LENGTH_SHORT).show();

                    int x = (int) pointValue.getX();
                    int y = (int) pointValue.getY();

                    int sec=0;
                    int totalSteps=0;
                    //Log.d(TAG, " on val selected " + pointValue);
                    Cursor cursor = db.getHealthyPaceByDate(x, YEAR);
                    cursor.moveToFirst();
                    //Log.d(TAG, sec + " " + totalSteps + " i is " + x);
                    while (cursor.isAfterLast() == false) {
                        try {

                            sec = Integer.parseInt(cursor.getString(0).toString());
                            totalSteps = Integer.parseInt(cursor.getString(1));
                            //Log.d(TAG, sec + " " + totalSteps + " i is " + i);
                        }
                        catch (Exception e) {
                            //Log.d(TAG, e.toString() + " on val selected");
                        }

                        cursor.moveToNext();

                    }
                    cursor.close();

                    //on value touch pass new object to new activity
                    AggregateInformation ai = new AggregateInformation();
                    ai.setType(query);
                    ai.setTotalSteps(y);
                    ai.setDate(x-1);
                    ai.setHealthyPaceSteps(totalSteps);
                    ai.setHealthyPaceDUration(sec);
                    ai.setCalsBurned(StepCounterActivity.calsPerStep(user, (double)y));
                    ai.setDistance((user.getStrideLength()*ai.getTotalSteps())/1000);

                    Intent intent = new Intent(getContext(), ScrollingActivity.class);
                    intent.putExtra("AggregateInformation", (Serializable) ai);
                    startActivity(intent);
                }

                @Override
                public void onValueDeselected() {

                }
            });
        }
        if(query.equals("weekly")){
            lineChartView.setOnValueTouchListener(new LineChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int i, int i1, PointValue pointValue) {

                    int x = (int) pointValue.getX();
                    int y = (int) pointValue.getY();

                    float sec=0;//TODO: change int
                    double totalSteps=0;

                    //double totalStepsForMandW=0;

                    int weekNumber=x;

                    Cursor cursor = db.getAverageHealthyPacePerWeek((weekNumber), YEAR);
                    cursor.moveToFirst();
                    while (cursor.isAfterLast() == false) {

                        //Log.d(TAG, cursor.getString(0) + " avgStep " + cursor.getString(1) + " avgDuration " + (weekNumber));

                        sec=Float.parseFloat(cursor.getString(1));
                        totalSteps=Double.parseDouble(cursor.getString(0));
                        //totalStepsForMandW = Double.parseDouble(cursor.getString(4));
                        cursor.moveToNext();
                    }
                    cursor.close();


                    //on clumnvalue touch pass new object to new activity and show more information there TODO: change to bundle
                    AggregateInformation ai = new AggregateInformation();
                    ai.setType("weekly");
                    ai.setTotalSteps(y);
                    ai.setDate(weekNumber);
                    ai.setHealthyPaceSteps((int)totalSteps);
                    ai.setHealthyPaceDUration((int)sec);
                    ai.setCalsBurned(StepCounterActivity.calsPerStep(user, y));
                    ai.setDistance((user.getStrideLength()*ai.getTotalSteps())/1000);
                    ai.setTotalStepsForMandW(weeklyTotals[weekNumber]);

                    Intent intent = new Intent(getContext(), ScrollingActivity.class);
                    intent.putExtra("AggregateInformation", (Serializable) ai);
                    startActivity(intent);

                }

                @Override
                public void onValueDeselected() {

                }
            });
        }
        if(query.equals("monthly")){
            //click column to view more infor about day/week/month etc.
            lineChartView.setOnValueTouchListener(new LineChartOnValueSelectListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onValueSelected(int i, int i1, PointValue pointValue){

                    Toast.makeText(getContext(),pointValue.getY()+"", Toast.LENGTH_SHORT).show();

                    int x = (int) pointValue.getX();//TODO: give real name
                    int y = (int) pointValue.getY();//TODO: give real name

                    //Log.d(TAG, x + " " + y + " i is " + i);

                    double sec=0;
                    double totalSteps=0;
                    //double totalStepsForMandW=0;

                    Cursor cursor = db.getAverageHealthyPacePerMonth((x+1), YEAR);
                    cursor.moveToFirst();
                    while (cursor.isAfterLast() == false) {
                        try {

                            sec = Double.parseDouble(cursor.getString(1).toString());
                            totalSteps = Double.parseDouble(cursor.getString(0));
                            //totalStepsForMandW = Double.parseDouble(cursor.getString(2));
                            //Log.d(TAG, sec + " " + totalSteps + " i month " + x);
                        }
                        catch (Exception e) {
                            //Log.d(TAG, e.toString() + " on val selected");
                        }

                        cursor.moveToNext();

                    }
                    cursor.close();

                    //on clumnvalue touch pass new object to new activity and show more information there
                    AggregateInformation ai = new AggregateInformation();
                    ai.setType(query);
                    ai.setTotalSteps(y);
                    ai.setDate(x);
                    ai.setHealthyPaceSteps((int) totalSteps);
                    ai.setHealthyPaceDUration((int) sec);
                    ai.setCalsBurned(StepCounterActivity.calsPerStep(user, (double)y));
                    ai.setDistance((user.getStrideLength()*ai.getTotalSteps())/1000);
                    ai.setTotalStepsForMandW(monthlyTotals[x]);

                    Intent intent = new Intent(getContext(), ScrollingActivity.class);
                    intent.putExtra("AggregateInformation", (Serializable) ai);
                    startActivity(intent);

                }

                @Override
                public void onValueDeselected() {

                }
            });
        }


        previewLineChartView.setZoomType(ZoomType.HORIZONTAL);
        previewLineChartView.setPreviewColor(Dashboard.colColor);

        lineChartView.setZoomEnabled(false);
        lineChartView.setScrollEnabled(true);


    }


    @Override
    public void onResume(){
        super.onResume();

        isPaused=false;

        //Log.d(TAG, "fragment2 onResume()");
    }

    @Override
    public void onPause(){
        super.onPause();

        isPaused=true;

        //Log.d(TAG, "fragment2 onPause()");
    }

    //invoked when fragment is visible
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
            if (isVisibleToUser) {


                //Log.d(TAG, "visible fragment 2");
            }
            else{


                //Log.d(TAG, "invisible fragment 2");
            }
        }

}