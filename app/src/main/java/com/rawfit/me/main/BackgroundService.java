package com.rawfit.me.main;

import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.IntentFilter;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.content.Intent;

import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Date;

import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.broadcastReceivers.ScreenOffReceiver;
import com.rawfit.me.broadcastReceivers.StepSensorChangedReceiver;
import com.rawfit.me.events.AccelerometerSensor;
import com.rawfit.me.events.Config;
import com.rawfit.me.events.ProximitySensor;
import com.rawfit.me.events.StepSensor;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class BackgroundService extends Service implements SensorEventListener {

    private static final String TAG = "BackgroundService";

    //sensors
    private SensorManager sensorManager;

    //db
    private SQLiteHelper db;

    //wakelock
    private PowerManager.WakeLock wakeLock;
    private ScreenOffReceiver screenOffReceiver;
    private StepSensorChangedReceiver stepSensorChangedReceiver;
    //private TimeChangedReceiver timeChangedReceiver;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        //Log.d(TAG, "onCreate() service");

        //starting foregorund so the service wont restart when closing app from most recent apps
        Notification notification = new Notification();
        startForeground(33, notification);


        //sqlitedb
        db = SQLiteHelper.getSqLiteHelper(this);


        //esper
        Esper.getEsperInstance(getApplicationContext(), getAssets());
        Esper.getEsper().getEngine().getEPRuntime().sendEvent(new Config("initCurrentHourFromDb", db.currentHourCount(System.currentTimeMillis())));

        //sensor manger and listeners
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);


        //query for startatboot option. if true start service at boot and if false dont do anything unless user decides to change settings
        Cursor cursor= db.getReadableDatabase().rawQuery("select stepSensorType from user", null);
        cursor.moveToFirst();


        int stepSensorType=-1;
        while (cursor.isAfterLast() == false) {
            if(cursor.getString(0)!=null) {
                    stepSensorType=Integer.parseInt(cursor.getString(0));
            }
            cursor.moveToNext();
        }
        cursor.close();

        //register listener for sensors
        //0 is default in this situation 1 is experimental, i.e, accelerometer with esper
        if(stepSensorType==0) {
            if (sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR) != null) {
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR), SensorManager.SENSOR_DELAY_FASTEST);
                //Log.d(TAG, "using TYPE_STEP_DETECTOR since user picked this option");
            } else if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                //Log.d(TAG, "using TYPE_ACCELEROMETER since TYPE_STEP_DETECTOR not supported");
                if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                    sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                    //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                }
            }
        }
        else if(stepSensorType==1){
            if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
                sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
                //Log.d(TAG, "using TYPE_ACCELEROMETER since user picked this option");
                if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                    sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), SensorManager.SENSOR_DELAY_NORMAL);
                    //Log.d(TAG, "using TYPE_PROXIMITY in combination with TYPE_ACCELEROMETER");
                }
            }
        }










        //wakelock or step sensor wont work
        PowerManager manager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

        screenOffReceiver = new ScreenOffReceiver(sensorManager, this);
        registerReceiver(screenOffReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));



        stepSensorChangedReceiver = new StepSensorChangedReceiver(sensorManager, this);
        registerReceiver(stepSensorChangedReceiver, new IntentFilter("stepSensorChanged"));



        //TODO: add restart of esper when time is chnaged manually since time cant go back in Esper
        //timeChangedReceiver = new TimeChangedReceiver();
        //registerReceiver(timeChangedReceiver, new IntentFilter(Intent.ACTION_TIME_CHANGED));


    }

    // service continues running until it is stopped
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "Step Counter Service Started", Toast.LENGTH_SHORT).show();
        wakeLock.acquire();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //db.close();
        TabFragment1.getCurrentHour();


        Esper.getEsper().getEngine().destroy();

        wakeLock.release();

        sensorManager.unregisterListener(this);

        //remove listeners
        unregisterReceiver(screenOffReceiver);
        unregisterReceiver(stepSensorChangedReceiver);

        Toast.makeText(this, "Step Counter Service Destroyed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        if (sensor.getType() == Sensor.TYPE_STEP_DETECTOR) {
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new StepSensor("STEP", System.currentTimeMillis(), event.values));
        }
        if (sensor.getType() == Sensor.TYPE_PROXIMITY) {
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new ProximitySensor("PROXIMITY", System.currentTimeMillis(), event.values));
        }
        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            Esper.getEsper().getEngine().getEPRuntime().sendEvent(new AccelerometerSensor("ACCELEROMETER", System.currentTimeMillis(), event.values));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public static Date getDate(long timestamp) {

        Date d = new Date(timestamp);

        return d;
    }



}
