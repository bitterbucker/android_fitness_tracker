package com.rawfit.me.main;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;

import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.stepcounter.StepCounterActivity;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.SubcolumnValue;

import static com.rawfit.me.stepcounter.StepCounterActivity.calsPerStep;
import static com.rawfit.me.stepcounter.StepCounterActivity.user;

public class Dashboard extends AppCompatActivity {

    public static int colColor;

    //public static final int colColor = Color.WHITE;

    private static final String TAG = "Dashboard";

    //requestcode when pasing user with intent to profile settings activity
    public static final int REQUESTCODE_USER = 1;

    //user profile
    public static User user;

    //db
    private static SQLiteHelper db;

    //counter
    public static double stepCounter;

    //TextViews
    private TextView stepTv = null;
    private TextView caloriesTv = null;

    //progressbars
    private ProgressBar stepPb;
    private ProgressBar calPb;

    private CardView cv;
    private CardView cv2;
    private CardView leftCard;
    private CardView rightCard;


    private boolean isPaused;






    //TODO: use subscriber object instead of update listener

    private Esper esper;

    private EPStatement getStepStmt;
    private UpdateListener stepListener;

    private EPStatement getNewMinuteStmt;
    private UpdateListener newMinuteListener;





    public static double getCalsForTheDay(){
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);

        int currentMinuteOfDay = ((hour * 60) + minute);

        int minsPerDay = 1440;

        double calsPrMin =  user.getBMR() / minsPerDay;

        double calsFromCurrentSteps = StepCounterActivity.calsPerStep(user, stepCounter);

        double calsburnedSoFar = calsPrMin * currentMinuteOfDay;//from bmr

        double totalCalsSoFar = calsburnedSoFar + calsFromCurrentSteps;//from bmr and steps taken

        return (int)totalCalsSoFar;
    }

    private void initCaloriesBurned() {

        double totalCalsSoFar = getCalsForTheDay();

        caloriesTv.setText("" + Math.round(totalCalsSoFar));

        calPb.setProgress((int) Math.round(totalCalsSoFar));

        //Log.d(TAG, "round " + Math.round(calsPrMin * currentMinuteOfDay) + " curstep " + calsFromCurrentSteps);
    }


    public static boolean useLeakCanary = false;
    public static boolean useAnimation = false;

    public static void initLeakCanary(Application application, Context context){
        if (useLeakCanary==true) {
            if (LeakCanary.isInAnalyzerProcess(context)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(application);
            // Normal app init code...
        }
    }



    public static void animate(Window window){
        if(useAnimation==true) {
            //animation
            //android.support.v4.widget.DrawerLayout constraintLayout = (android.support.v4.widget.DrawerLayout) findViewById(R.id.drawer_layout);
            //https://stackoverflow.com/questions/14289863/find-root-layout-in-activity
            AnimationDrawable animationDrawable = (AnimationDrawable) window.getDecorView().getRootView().getBackground();
            animationDrawable.setEnterFadeDuration(2000);//2000
            animationDrawable.setExitFadeDuration(4000);//4000
            animationDrawable.start();
        }
    }

    class T implements Runnable{

        public Context c;
        public T(Context c) {
            this.c=c;
        }

        @Override
        public void run() {
            SQLiteHelper db = SQLiteHelper.getSqLiteHelper(c);
            db.genRandomHourlyData();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLeakCanary(getApplication(), this);

        colColor = ContextCompat.getColor(this, R.color.primary_white);


        setContentView(com.rawfit.me.main.R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(com.rawfit.me.main.R.id.toolbar);
        setSupportActionBar(toolbar);

        //animation
        animate(getWindow());


        //db
        db = SQLiteHelper.getSqLiteHelper(getApplicationContext());

        //random data
        //for (int i =0;i<1;i++) {new Thread(new T(getApplicationContext())).start();}

        //esper
        esper = Esper.getEsperInstance(getApplicationContext(), getAssets());




        getStepStmt = esper.getEngine().getEPAdministrator().getStatement("step");
        stepListener = new UpdateListener() {
            @Override
            public void update(EventBean[] eventBeen, EventBean[] eventBeen1) {

                stepCounter++;

                final TextView steptv=(TextView) findViewById(R.id.stepTv);
                final ProgressBar pb = (ProgressBar) findViewById(R.id.stepProgressBar);

                steptv.post(new Runnable() {
                    @Override
                    public void run() {
                        steptv.setText(""+ (int)stepCounter);
                        pb.setProgress((int)stepCounter);
                        //Log.d(TAG, "step dashboard");
                    }
                });

            }
        };


        getNewMinuteStmt = esper.getEngine().getEPAdministrator().getStatement("newMinute");
        newMinuteListener = new UpdateListener() {
            @Override
            public void update(EventBean[] eventBeen, EventBean[] eventBeen1) {

                final TextView caltv=(TextView) findViewById(R.id.caloriesTv);
                final ProgressBar pb = (ProgressBar) findViewById(R.id.calProgressBar);

                caltv.post(new Runnable() {
                    @Override
                    public void run() {
                        caltv.setText(""+ Math.round(getCalsForTheDay()));
                        pb.setProgress( (int) Math.round(getCalsForTheDay()));
                        //Log.d(TAG, "step dashboard");
                    }
                });

            }
        };

        //add listeners to stmts
        getStepStmt.addListener(stepListener);
        getNewMinuteStmt.addListener(newMinuteListener);


        //user
        user = db.getUser();
        //Log.d(TAG, "BMR "+user.getBMR());
        //Log.d(TAG, "User "+user.toString());


        /*Cursor cursor=db.getReadableDatabase().rawQuery("select weight, timestamp from weight",null);
        while (cursor.isAfterLast() == false) {
            try {

                Log.d(TAG, cursor.getString(0) +" "+ cursor.getString(1));

            } catch (Exception e) {
                System.err.print(e.toString());
            }
            cursor.moveToNext();
        }
        cursor.close();*/



        //card view
        cv = (CardView) findViewById(com.rawfit.me.main.R.id.step_card_dashboard);
        cv2 = (CardView) findViewById(com.rawfit.me.main.R.id.calories_card_dashboard);
        leftCard = (CardView) findViewById(R.id.left_card);
        rightCard = (CardView) findViewById(R.id.right_card);

        leftCard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    leftCard.setCardBackgroundColor(getResources().getColor(R.color.transparent));
                    return true;
                } else {
                    leftCard.setCardBackgroundColor(getResources().getColor(R.color.transparent2));
                }

                return false;
            }
        });

        rightCard.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    rightCard.setCardBackgroundColor(getResources().getColor(R.color.transparent));
                    return true;
                } else {
                    rightCard.setCardBackgroundColor(getResources().getColor(R.color.transparent2));
                }
                return false;
            }
        });

        cv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    cv.setCardBackgroundColor(getResources().getColor(R.color.transparent3));

                    Intent intent = new Intent(getApplicationContext(), StepCounterActivity.class);
                    intent.putExtra("userInfo", user);
                    startActivity(intent);

                    return true;
                } else {
                    cv.setCardBackgroundColor(getResources().getColor(R.color.transparent));
                }

                return false;
            }
        });





        //random data
        //db.getWritableDatabase().execSQL("delete from healthyPace");db.close();db.getWritableDatabase().execSQL("delete from achievement");db.close();db.truncateStepTable();db.close();




        //text views
        stepTv = (TextView) findViewById(R.id.stepTv);
        caloriesTv = (TextView) findViewById(R.id.caloriesTv);

        //pb
        stepPb=(ProgressBar) findViewById(R.id.stepProgressBar);
        stepPb.setMax(user.getStepGoal());
        stepPb.setIndeterminate(false);

        calPb=(ProgressBar) findViewById(R.id.calProgressBar);
        calPb.setMax((int)user.getCalorieBurnedPerDayGoal());
        calPb.setIndeterminate(false);

        new DoInBg().execute("totalSteps");


        if (isMyServiceRunning(BackgroundService.class) == false) {

            //starting service
            Intent service = new Intent(getBaseContext(), BackgroundService.class);
            startService(service);

            //Log.d(TAG, "service started from main activity");
            Toast.makeText(this, "Starting Background Service", Toast.LENGTH_SHORT).show();
        } else {
            //Toast.makeText(this, "Service Already Running", Toast.LENGTH_SHORT).show();
        }


        isPaused=false;

        TabFragment1.getCurrentHour();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        //Log.d(TAG, "onActivityResult()" + requestCode + " " + resultCode);
        if (requestCode == REQUESTCODE_USER) {
            // Make sure the request was successful
            if (resultCode == REQUESTCODE_USER) {
                user = (User) data.getSerializableExtra("userInfoUpdate");
                stepTv.setText(stepCounter+"");

                stepPb.setMax(user.getStepGoal());
                calPb.setMax((int)user.getCalorieBurnedPerDayGoal());

                //Log.d(TAG, user.toString() + " user updated");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onResume(){
        super.onResume();

        if(isPaused==true){
            //register listener
            getStepStmt.addListener(stepListener);
            getNewMinuteStmt.addListener(newMinuteListener);

            TabFragment1.getCurrentHour();

            new DoInBg().execute("totalSteps");

            //Log.d(TAG, "fragment onResume() - re-register listeners");
        }
        else{



            //Log.d(TAG, "fragment onResume()");
        }


    }

    @Override
    public void onPause(){
        super.onPause();

        TabFragment1.getCurrentHour();

        isPaused=true;

        //unregister all listeners otherwise you will receive multiple updates when statements fires
        getStepStmt.removeListener(stepListener);
        getNewMinuteStmt.removeListener(newMinuteListener);


        //Log.d(TAG, "onPause()");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.info) {

            return true;
        }
        if (id == R.id.profile_button) {

            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("userInfo", user);
            startActivityForResult(intent, REQUESTCODE_USER);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    //method invocation for esper
    public static User getdailyGoal() {

        int year = java.util.Calendar.getInstance().get(java.util.Calendar.YEAR);
        int day = java.util.Calendar.getInstance().get(java.util.Calendar.DAY_OF_YEAR);
        int achievmentCount = db.getAchievement(day, year);

        //Log.d(TAG, "getdailyGoal()");

        Cursor cursor = db.getTotalSteps();
        cursor.moveToFirst();
        int currentStepCount = 0;
        while (cursor.isAfterLast() == false) {
            try {
                //Log.d(TAG, cursor.getString(0));
                currentStepCount = Integer.parseInt(cursor.getString(0));
            } catch (Exception e) {
                System.err.print(e.toString());
            }
            cursor.moveToNext();
        }
        cursor.close();

        //O o = new O();
        //TODO: query db for user instead
        user.setCurrentStep(currentStepCount);
        user.setCurrentDayAchievementCount(achievmentCount);


        return user;
    }


    public static O getStats(){

        Cursor cursor = db.getTotalSteps();
        cursor.moveToFirst();
        int stepCount = 0;
        while (cursor.isAfterLast() == false) {
            try {
                //Log.d(TAG, cursor.getString(0));
                stepCount = Integer.parseInt(cursor.getString(0));
            } catch (Exception e) {
                System.err.print(e.toString());
            }
            cursor.moveToNext();
        }
        cursor.close();


        O o = new O();

        o.setCals( (int)(StepCounterActivity.calsPerStep(user, stepCount) + user.getBMR()));
        o.setWeight((int)user.getWeight());

        return o;
    }



    private class DoInBg extends AsyncTask<String, Integer, Cursor> {


        String query;


        @Override
        protected void onPreExecute() {

            //reset counters
            stepCounter=0;
            //currentHourlyCount=0;

            //Log.d(TAG," Task Starting...");
        }


        @Override
        protected Cursor doInBackground(String... params) {

            if(params[0].equals("totalSteps")){
                query="totalSteps";
                return db.getTotalSteps();
            }

            return null;
        }


        @Override
        protected void onProgressUpdate(Integer... params) {

        }

        @Override
        protected void onPostExecute(Cursor result) {

            if(query.equals("totalSteps")){
                double stepCount=0;
                result.moveToFirst();
                while (result.isAfterLast() == false) {
                    if(result.getString(0)!=null){
                        stepCount = Double.parseDouble(result.getString(0));
                        //Log.d(TAG,stepCount + " total steps");
                        stepCounter += (int) stepCount;

                        //Log.d(TAG,"current step set to " + stepCounter);
                    }
                    result.moveToNext();
                }
                result.close();

                user.setCurrentStep((int)stepCounter);

                stepTv.setText((int)stepCounter+"");
                stepPb.setProgress((int)stepCounter);

                calPb.setMax((int)user.getCalorieBurnedPerDayGoal());
                initCaloriesBurned();

            }
        }
    }


}
