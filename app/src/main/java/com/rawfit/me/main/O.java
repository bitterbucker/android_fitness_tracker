package com.rawfit.me.main;

public class O {

    private int cals;
    private int currentStep;
    private int currentDayAchievementCount;
    private int weight;
    private int stepGoal;
    private double calorieBurnedPerDayGoal;

    public int getStepGoal() {
        return stepGoal;
    }

    public void setStepGoal(int stepGoal) {
        this.stepGoal = stepGoal;
    }

    public double getCalorieBurnedPerDayGoal() {
        return calorieBurnedPerDayGoal;
    }

    public void setCalorieBurnedPerDayGoal(double calorieBurnedPerDayGoal) {
        this.calorieBurnedPerDayGoal = calorieBurnedPerDayGoal;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getCals() {
        return cals;
    }

    public void setCals(int cals) {
        this.cals = cals;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }

    public int getCurrentDayAchievementCount() {
        return currentDayAchievementCount;
    }

    public void setCurrentDayAchievementCount(int currentDayAchievementCount) {
        this.currentDayAchievementCount = currentDayAchievementCount;
    }
}
