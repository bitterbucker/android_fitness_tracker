package com.rawfit.me.main;

import java.io.Serializable;


public class User implements Serializable{

    private String name;
    private double weight;
    private double height;
    private int stepGoal;
    private double calorieBurnedPerDayGoal;
    private String unit;
    private Gender gender;
    private double stride;
    private int age;
    private int startAtBoot;
    private int stepSensorType;

    public User() {

    }

    public enum Gender {
        UNKNOWN("U"), MALE("M"), FEMALE("F");

        private String val;

        public String getval() {
            return val;
        }

        private Gender(String val) {
            this.val = val;
        }

    }

    public double getBMR() {

        int a = age;
        double h = height;
        double m = weight;

        if (unit.equals("imperial")) {
            h = Converter.inToCm(h);
            m = Converter.lbsToKg(m);
        }

        if (gender == Gender.MALE) {
            //male
            return (13.7516 * m / 1) + (5.0033 * h / 1) - (6.7550 * a / 1) + 66.4730;
        }

        if (gender == Gender.FEMALE) {
            //female
            return (9.5634 * m / 1) + (1.8496 * h / 1) - (4.675 * a / 1) + 655.0955;
        }

        return -1;
    }

    //TODO: store this information elsewhere when used with esper
    private int currentStep;
    private int currentDayAchievementCount;

    public int getCurrentDayAchievementCount() {
        return currentDayAchievementCount;
    }

    public void setCurrentDayAchievementCount(int currentDayAchievementCount) {
        this.currentDayAchievementCount = currentDayAchievementCount;
    }

    public int getCurrentStep() {
        return currentStep;
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
    }
    //TODO: ------------------------------------

    //stride length
    private double getStride(){
        double heightInM=0;

        if(unit.equals("imperial")){
            heightInM = Converter.inToCm(getHeight())/100;
        }else {
            heightInM = getHeight() / 100;
        }

        if(gender==Gender.MALE){
            return heightInM*0.415;
        }
        if(gender==Gender.FEMALE) {
            return heightInM * 0.413;
        }

        return heightInM*0.414;
    }

    public double getStrideLength() {

        if(stride!=0){
            //Log.d("User", "Stride != 0 " + stride);
            return stride;
        }

        return getStride();
    }

    public int getStartAtBoot() {
        return startAtBoot;
    }

    public void setStartAtBoot(int startAtBoot) {
        this.startAtBoot = startAtBoot;
    }

    public int getStepSensorType() {
        return stepSensorType;
    }

    public void setStepSensorType(int stepSensorType) {
        this.stepSensorType = stepSensorType;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setStride(double stride) {
        this.stride = stride;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getStepGoal() {
        return stepGoal;
    }

    public void setStepGoal(int stepGoal) {
        this.stepGoal = stepGoal;
    }

    public double getCalorieBurnedPerDayGoal() {
        return calorieBurnedPerDayGoal;
    }

    public void setCalorieBurnedPerDayGoal(double calorieBurnedPerDayGoal) {
        this.calorieBurnedPerDayGoal = calorieBurnedPerDayGoal;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                ", height=" + height +
                ", stepGoal=" + stepGoal +
                ", calorieBurnedPerDayGoal=" + calorieBurnedPerDayGoal +
                ", unit='" + unit + '\'' +
                ", gender=" + gender +
                ", stride=" + stride +
                ", age=" + age +
                ", currentStep=" + currentStep +
                ", currentDayAchievementCount=" + currentDayAchievementCount +
                '}';
    }
}
