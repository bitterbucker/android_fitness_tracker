package com.rawfit.me.main;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.rawfit.me.SQL.SQLiteHelper;
import com.rawfit.me.stepcounter.Achievement;

import static com.rawfit.me.main.Dashboard.initLeakCanary;


public class TabFragment3 extends Fragment {

    private static final String TAG = "TabFragment3";

    //sqlite db
    public SQLiteHelper db;
    private ListView lv;
    private ArrayAdapter<String> listAdapter;

    private List<Achievement> achievementList;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //Log.d(TAG, "onCreate() fragment");

        initLeakCanary(getActivity().getApplication(), getContext());


        //db
        db = SQLiteHelper.getSqLiteHelper(getContext());

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        refreshList(view);






    }

    private void refreshList(View view){
        //find the ListView resource.
        lv = (ListView) view.findViewById(R.id.mainListView );

        achievementList = db.getAchievement();


        // Create and populate a List of planet names.
        String[] aToString = new String[achievementList.size()];

        for (int i=0;i<achievementList.size();i++) {
            aToString[i] = achievementList.get(i).toString();
        }

        ArrayList<String> aList = new ArrayList<String>();
        aList.addAll( Arrays.asList(aToString) );


        RecyclerView.ViewHolder holder = null;


        // Create ArrayAdapter using the list.
        listAdapter = new ArrayAdapter<String>(getContext(), R.layout.simplerow, aList);

        // Set the ArrayAdapter as the ListView's adapter.
        lv.setAdapter( listAdapter );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.tab_fragment_3, container, false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            refreshList(getView());

            //Log.d(TAG, "visible fragment 3");
        } else {


            //Log.d(TAG, "invisible fragment 3");
        }
    }
}