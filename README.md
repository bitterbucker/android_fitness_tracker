# Android Fitness Tracker #


## Description ##

Experimental fitness app leveraging Complex Event Processing(CEP) to process sensor data from smartphones to detect patterns in movement to detect steps, healthy pace and inactivity.


## Features ##

The app has the following features atm:

### Stepcounter ###

This app uses a CEP engine called Esper to process sensor data from the built-in sensors on Android devices.
	
CEP is used to detect:

* Inactivity
* Healthy pace
* Steps

	
### Calorie expendage ###

Calories burned based of your BMR and calories burned from the stepdetector.


## Installation ##

Clone and run from android studio or download from Google Play:

* https://play.google.com/store/apps/details?id=com.rawfit.me.main




## Screenshots ##

			
![picture](img/1.png)
![picture](img/2.png)
![picture](img/3.png)
![picture](img/4.png)
![picture](img/5.png)
![picture](img/6.png)


## Lisence ##

Licensed under The GNU General Public License (GPL-2.0)

http://opensource.org/licenses/gpl-2.0.php